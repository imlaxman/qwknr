<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/style-ref.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/flexslider.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/resources/appResJS/js/jquery.min.js" type="text/javascript"></script>

</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<br><br><br><br><br><br>
<div class="login-page">
		<div class="container">
			<div class="account_grid">
			
			   <div class="col-md-6 login-right wow fadeInRight" data-wow-delay="0.4s">
					<h3>REGISTERED CUSTOMERS</h3>
					<p>If you have an account with us, please log in.</p>
					 <form:form method="POST" commandName="formBean" action="admin-user-login">  
						<div>
							<span>Email Address<label>*</label></span>
							<form:input path="loginEmail" cssClass="text" />
						<form:errors path="loginEmail" cssClass="errors"/>
						</div>
						<div>
							<span>Password<label>*</label></span>
							<form:password path="loginPassword" cssClass="text" />
						<form:errors path="loginPassword" cssClass="errors"/>
						</div>
						<a class="forgot" href="#">Forgot Your Password?</a>
						<!-- <input type="submit" value="Login"> -->
						<button name="submit" type="submit"  class="acount-btn"  id="contact-submit">Login</button>
					</form:form>
			   </div>	
			   	<div class="col-md-6 login-left wow fadeInLeft" data-wow-delay="0.4s">
					<h3>NEW to Qws</h3>
					<p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
					<a class="acount-btn" href="user-reg">Create an Account</a>
			   </div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>