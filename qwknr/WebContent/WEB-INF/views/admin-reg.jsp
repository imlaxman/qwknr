<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/style-ref.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/flexslider.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/resources/appResJS/js/jquery.min.js" type="text/javascript"></script>

</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<br><br><br><br><br><br>
<div class="main-1">
	
		<div class="container">
		
			<div class="register">
			<div class="col-sm-10xxx">
		  	 <form:form method="POST" commandName="formBean" action="admin-reg">  
				 <div class="register-top-grid">
				 <div class="col-sm-12">
					<h3>PERSONAL INFORMATION</h3>
					 <div class="wow fadeInLeft" data-wow-delay="0.4s">
						<span>First Name<label>*</label></span>
						<form:input path="firstName" cssClass="text" />
						<form:errors path="firstName" cssClass="errors"/>
						<!-- <input type="text">  -->
					 </div>
					 <div class="wow fadeInRight" data-wow-delay="0.4s">
						<span>Last Name<label>*</label></span>
						<form:input path="lastName" cssClass="text" />
						<form:errors path="lastName" cssClass="errors"/>
					 </div>
					 <div class="wow fadeInRight" data-wow-delay="0.4s">
						 <span>Email Address<label>* (login id)</label></span>
						 <form:input path="loginEmail" cssClass="text" />
						<form:errors path="loginEmail" cssClass="errors"/>
					 </div>
					 <div class="wow fadeInRight" data-wow-delay="0.4s">
						 <span>Mobile<label>*</label></span>
						 <form:input path="mobile" cssClass="text" />
						<form:errors path="mobile" cssClass="errors"/>
					 </div>
					 	 <div class="wow fadeInLeft" data-wow-delay="0.4s">
								<span>Password<label>*</label></span>
								<form:password path="loginPassword" cssClass="text" />
						<form:errors path="loginPassword" cssClass="errors"/>
							 </div>
							 <div class="wow fadeInRight" data-wow-delay="0.4s">
								<span>Confirm Password<label>*</label></span>
								<form:password path="confirmPassword" cssClass="text" />
						<form:errors path="confirmPassword" cssClass="errors"/>
							 </div>
						
						 <div class="register-but">
					  <!--  <input type="submit" value="submit"> -->
					   <button name="submit" type="submit"  class="text"  id="contact-submit">Submit</button>
								</div>		
								</div>
					<!--  <div class="clearfix"> </div>
					   <a class="news-letter" href="#">
						 <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i> </i>Sign Up for Newsletter</label>
					   </a> -->
					 </div>
					
				   
				</form:form>
			
		   </div>
		   </div>
		 </div>
	</div>
	
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>