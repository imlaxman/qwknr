<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/style-ref.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/flexslider.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/resources/appResJS/js/jquery.min.js" type="text/javascript"></script>

</head>
<body>

<jsp:include page="header.jsp"></jsp:include>


<div class="main-head-section">
		 		<div class="container">
		 			<h3>contact</h3>
		 		</div>
		 </div>
		
		<div class="contact_top">
			 		<div class="container">
			 			<div class="col-md-8 contact_left">
			 				<h4>Contact Form</h4>
			 				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt dolor et tristique bibendum. Aenean sollicitudin vitae dolor ut posuere.</p>
							  <form>
								 <div class="form_details">
					                 <input type="text" class="text" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
									 <input type="text" class="text" value="Email Address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email Address';}">
									 <input type="text" class="text" value="Subject" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Subject';}">
									 <textarea value="Message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
									 <div class="clearfix"> </div>
									 <div class="sub-button">
									 	<input type="submit" value="Send message">
									 </div>
						          </div>
						       </form>
					        </div>
					        <div class="col-md-4 company-right">
					        	<div class="company_ad">
							     		<h3>Contact Info</h3>
							     		<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit velit justo.</span>
			      						<address>
											 <p>email:<lable>info@display.com</lable></p>
											 <p>phone: 1.306.222.4545</p>
									   		<p>222 2nd Ave South</p>
									   		<p>Saskabush, SK   S7M 1T6</p>
									 	 	
							   			</address>
							   		</div>
							   		<div class="store">
							   			<h4>Store Hours</h4>
							   			<div class="store-data">
							   			<div class="col-md-6 days">
							   				<p>Monday - Thursday</p>
							   				<p>Friday</p>
							   				<p>Saturday </p>
							   				<p>Sunday & Holidays</p>
							   				
							   			</div>
							   			<div class="col-md-6 hours">
							   				<p> 8 am - 5 pm</p>
							   				<p>8 am - 6 pm</p>
							   				<p> 9 am - 5 pm</p>
							   				<p> Closed</p>
							   			</div>
							   			<div class="clearfix"> </div>  
							   		</div>
						    </div>				
							</div>	
						</div>
					</div>



<jsp:include page="footer.jsp"></jsp:include>


</body>
</html>