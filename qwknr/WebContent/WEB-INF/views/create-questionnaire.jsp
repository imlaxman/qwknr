<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/style-ref.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/flexslider.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/responsiveslides.min.js" type="text/javascript"></script>
</head>
<body>

<jsp:include page="header.jsp"></jsp:include>


<div class="main-head-section">
		 		<div class="container">
		 			<h3></h3>
		 		</div>
		 </div>


	<div class="contact_top">
		<div class="container">
		<div class="col-md-1 contact_left">
		</div>
			<div class="col-md-6 contact_left">
				<h4>Questionnaire Posting Form</h4>
				<p>Post Question here</p>
				<form:form method="POST" commandName="questionnaireFormBean" action="create-questionnaire">
					<div class="form-group">
						<label for="question">Question</label>
						<form:input path="question" cssClass="form-control" id="inputEmail" />
						<form:errors path="question" cssClass="errors" />
					</div>
					<div class="form-group">
						<label for="option1">Option-1</label>
						<form:input path="option1" cssClass="form-control" id="inputPassword" />
						<form:errors path="option1" cssClass="errors" />
					</div>
					<div class="form-group">
						<label for="option2">Option-2</label>
						<form:input path="option2" cssClass="form-control" id="inputEmail" />
						<form:errors path="option2" cssClass="errors" />
					</div>
					<div class="form-group">
						<label for="option3">Option-3</label>
						<form:input path="option3" cssClass="form-control" id="inputPassword" />
						<form:errors path="option3" cssClass="errors" />
					</div>
					<div class="form-group">
						<label for="option4">Option-4</label>
						<form:input path="option4" cssClass="form-control" id="inputEmail" />
						<form:errors path="option4" cssClass="errors" />
					</div>
					<div class="form-group">
						<label for="answer">Answer</label>
						<%-- <form:input path="answer" cssClass="form-control" id="answer" /> --%>
						
						<form:select type="text" path="answer" class="form-control" placeholder="answer">
		        <form:option cssStyle="color:#eee;" value="" label="--- Answer ---"/>
		         <form:options items="${answersList}" />
		        </form:select>
		        
						<form:errors path="answer" cssClass="errors" />
					</div>
					<div class="form-group">
						<label for="questionCategory">Category</label>
					<%-- 	<form:input path="questionCategory" cssClass="form-control" id="inputEmail" /> --%>
						<form:select type="text" path="questionCategory" class="form-control" placeholder="questionCategory">
		        <form:option cssStyle="color:#eee;" value="" label="--- Category ---"/>
		         <form:options items="${categoriesList}" />
		        </form:select>
						<form:errors path="questionCategory" cssClass="errors" />
					</div>
					<div class="form-group">
						<label for="questionLevelName">Level</label>
						<%-- <form:input path="questionLevelName" cssClass="form-control" id="inputPassword" /> --%>
						<form:select type="text" path="questionLevelName" class="form-control" placeholder="questionLevelName">
		        <form:option cssStyle="color:#eee;" value="" label="--- questionLevelName ---"/>
		         <form:options items="${levelsList}" />
		        </form:select>
						<form:errors path="questionLevelName" cssClass="errors" />
					</div>
					<div class="form-group">
						<!-- <button name="submit" type="submit" class="sub-button" id="contact-submit">Submit</button> -->
						<input type="submit" value="Sumbit">
					</div>
				</form:form>
			</div>
		</div>
	</div>



	<div class="main-head-section">
		 		<div class="container">
		 			<h3></h3>
		 		</div>
		 </div>


<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>