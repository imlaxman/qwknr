<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="UTF-8">
    
     <link type="text/css" href="${pageContext.request.contextPath}/resources/css/styleform.css" rel="stylesheet">
      <!--[if lt IE 9]>
	      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
      <![endif]-->
   </head>
   <body class="size-1140">
      <!-- TOP NAV WITH LOGO -->
      <header>
         
         <nav>
            <div class="line">
               <div class="s-12 l-2">
                  <p class="logo"><strong>Quixing</strong></p>
               </div>
               <div class="top-nav s-12 l-10">
                  <p class="nav-text">Custom menu text</p>
                  <ul class="right">
                     <li class="active-item"><a href="#carousel">Home</a></li>
                     <li><a href="#features">Features</a></li>
                     <li><a href="about-us">About Us</a></li>
                     <li><a href="#our-work">Our Work</a></li>
                     <li><a href="#services">Services</a></li>
                     <li><a href="contact">Contact</a></li>
                  </ul>
               </div>
               <div class="header-right1">
						<li><a href="user-login">Sign in</a></li>
						<li><a href="user-reg">Sign up</a></li>
						<li><a href="adminxx">Admin</a></li>
					</div>
            </div>
         </nav>
      </header>  
      <section>
        
      <script type="text/javascript" src="owl-carousel/owl.carousel.js"></script>
      <script type="text/javascript">
         jQuery(document).ready(function($) {
            var theme_slider = $("#owl-demo");
            $("#owl-demo").owlCarousel({
                navigation: false,
                slideSpeed: 300,
                paginationSpeed: 400,
                autoPlay: 6000,
                addClassActive: true,
             // transitionStyle: "fade",
                singleItem: true
            });
            
        
            // Custom Navigation Events
            $(".next-arrow").click(function() {
                theme_slider.trigger('owl.next');
            })
            $(".prev-arrow").click(function() {
                theme_slider.trigger('owl.prev');
            })     
        }); 
      </script>
   </body>
</html>