<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/style-ref.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/flexslider.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/resources/appResJS/js/jquery.min.js" type="text/javascript"></script>


</head>
<body>
<jsp:include page="header.jsp"></jsp:include>



<div class="mission">
		<div class="container">
			<h3>Question</h3>
		<div class="mission-grids">
			<div class="col-md-6 mbtm50">
					<div class="col-md-10">
				 <input type="checkbox" class="checkbox"> 	<p>Option 1</p>
				</div>
					<div class="clearfix"> </div>  
			 </div>
			
			 <div class="col-md-6 mbtm50">
					<div class="col-md-10">
					 <input type="checkbox" class="checkbox">
					<p>Option 2</p>
				</div>
					<div class="clearfix"> </div>  
		   </div>
		
		<div class="col-md-6 mbtm50">
					<div class="col-md-10">
					 <input type="checkbox" class="checkbox">
					<p>Option 3</p>
				</div>
					<div class="clearfix"> </div>  
		</div>
		
		
		<div class="col-md-6 mbtm50">
					<div class="col-md-10">
					 <input type="checkbox" class="checkbox">
					<p>Option 4</p>
				</div>
					<div class="clearfix"> </div>  
		</div>
		<div class="clearfix"> </div>  
	</div>
	<div class="sub-button">
									 	<input type="submit" value="Submit">
									 </div>
									 
	</div>
</div>








<div id="services">
            <div class="line">
               <h2 class="section-title">What we do</h2>
               <div class="margin">
                  <div class="s-12 m-6 l-4 margin-bottom">
                     <i class="icon-vector"></i>
                     <div class="service-text">
                        <h3>We create</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                     </div>
                  </div>
                  <div class="s-12 m-6 l-4 margin-bottom">
                     <i class="icon-eye"></i>
                     <div class="service-text">
                        <h3>We look to the future</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                     </div>
                  </div>
                  <div class="s-12 m-12 l-4 margin-bottom">
                     <i class="icon-random"></i>
                     <div class="service-text">
                        <h3>We find a solution</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>


<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>