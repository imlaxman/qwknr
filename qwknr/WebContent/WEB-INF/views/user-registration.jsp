<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/style-ref.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/flexslider.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/resources/appResJS/js/jquery.min.js" type="text/javascript"></script>


</head>
<body>

<jsp:include page="header.jsp"></jsp:include>

<!-- registration -->
	<div class="main-1">
	
		<div class="container">
		
			<div class="register">
			<div class="col-sm-10xxx">
		  	 <form:form method="POST" commandName="formBean" action="user-temp-reg">  
				 <div class="register-top-grid">
				 <div class="col-sm-12">
					<h3>PERSONAL INFORMATION</h3>
					 <div class="wow fadeInLeft" data-wow-delay="0.4s">
						<span>First Name<label>*</label></span>
						<form:hidden path="email"/>
						<form:hidden path="token"/>
						<!-- <input type="text">  -->
					 </div>
				</div>
					 </div>
					
				   
				</form:form>
			
		   </div>
		   </div>
		 </div>
	</div>
<!-- registration -->


<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>