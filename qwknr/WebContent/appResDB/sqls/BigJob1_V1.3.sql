--QUERIES EXECUTED ON 24 JUNE 2013
CREATE TABLE `JSK_PRFL_DTL` (                                  
           `JSK_ID` BIGINT(10) NOT NULL AUTO_INCREMENT,             
           `FIRST_NAME` VARCHAR(32) DEFAULT NULL,       
           `LAST_NAME` VARCHAR(32) DEFAULT NULL,               
           `MOBILE` VARCHAR(14) DEFAULT NULL,                   
           `RESUME_TITLE` VARCHAR(32) DEFAULT NULL,                
           `EXPERIENCE` DECIMAL(2,0) DEFAULT NULL,             
           `KEY_SKILLS` VARCHAR(256) DEFAULT NULL,             
           `FUNCTIONAL_AREA` VARCHAR(64) DEFAULT NULL,         
           `EDUCATION` VARCHAR(6) DEFAULT NULL,                
           `MASTERS_EDUCATION` VARCHAR(6) DEFAULT NULL,        
           `CERTIFICATIONS` VARCHAR(128) DEFAULT NULL,         
           `DESIGNATION` VARCHAR(32) DEFAULT NULL,                 
           `CURRENT_COMPANY` VARCHAR(32) DEFAULT NULL,             
           `CURRENT_SALARY` DECIMAL(7,2) DEFAULT NULL,             
           `EXPECTED_SALARY` DECIMAL(7,2) DEFAULT NULL,            
	       `LOCATION` VARCHAR(32) DEFAULT NULL,                
           `WILLING_TO_RELOCATE` VARCHAR(8) DEFAULT NULL,
	       `PRFL_CREATE_DATE` DATETIME DEFAULT NULL,                    
           `PRFL_LAST_UPDATE_DATE` DATETIME DEFAULT NULL,                          
			PRIMARY KEY  (`JSK_ID`)
         ) 
          
         
CREATE TABLE `JSK_LOGIN` (
			 `JSK_ID` BIGINT(10) NOT NULL,
             `EMAIL` VARCHAR(50) NOT NULL default'',            
             `PASSWORD` VARCHAR(12) DEFAULT NULL,
	      	 `USER_TYPE` VARCHAR(12) DEFAULT NULL,
              PRIMARY KEY (`JSK_ID`),
  			  UNIQUE KEY `JSK_ID` (`JSK_ID`)
           ) 
           
CREATE TABLE `JSK_FILES` (
		 `JSK_ID` BIGINT(10) NOT NULL,
		 `CV` BLOB,                         
		 `IMAGE` BLOB,                      
		  PRIMARY KEY (`JSK_ID`),
  		  UNIQUE KEY `JSK_ID` (`JSK_ID`)                    
           )
           
           
CREATE TABLE `JSK_TERMS_AGREED` (
		`JSK_ID` BIGINT(10) NOT NULL AUTO_INCREMENT,
		`JOB_ALERTS` CHAR(1) DEFAULT '0',                       
        `NOTIFICATION_SERVICE_EMAILS` CHAR(1) DEFAULT '0',     
        `OTHER_PROMOTION_SPECIAL_OFFERS` CHAR(1) DEFAULT '0',  
		`PRIVACY_POLICY` VARCHAR(50) NOT NULL default '',
		 CONSTRAINT `FKJSKR3` FOREIGN KEY (`JSK_ID`) REFERENCES `JSK_PRFL_DTL` (`JSK_ID`)                                                                                
		 )
		 
CREATE TABLE `JSK_JOB_APPLICATIONS` (                           
		`JOB_APPLICATION_ID` BIGINT(10) NOT NULL AUTO_INCREMENT,  
		`JOB_POSTING_ID` BIGINT(10) NOT NULL,                      
		`JSK_ID` BIGINT(10) NOT NULL,                              
		`JOB_APPLIED_DATE` DATETIME DEFAULT NULL,                 
		PRIMARY KEY  (`JOB_APPLICATION_ID`),                      
		CONSTRAINT `FKJSKR4` FOREIGN KEY (`JSK_ID`) REFERENCES `JSK_PRFL_DTL` (`JSK_ID`)
        ) 
           
CREATE TABLE `JSKR_PRFL_VIEW_DETAILS` (                           
		`JSK_ID` BIGINT(6) NOT NULL,
		`EMPLR_ID` BIGINT(6) NOT NULL,
		`JSKR_PRFL_VIEW_DATE` DATETIME DEFAULT NULL
		)     
		
CREATE TABLE `JSKR_PROJECT_DETAILS` (
		`JSKR_PROJECTS_ID` BIGINT(10) NOT NULL AUTO_INCREMENT,
		`JSK_ID` BIGINT(10) NOT NULL,                                      
		`PRJ_NUM` BIGINT(6) NOT NULL ,  
		`PRJ_NAME` VARCHAR(64) NOT NULL,					
		`PRJ_FROM_DATE` VARCHAR(64) NOT NULL,                      
		`PRJ_TO_DATE` VARCHAR(64) NOT NULL,                                                             
		`PRJ_DURTN` VARCHAR(64) NOT NULL,                                                             
		`ROLE_IN_PRJ` VARCHAR(64) NOT NULL,
		`JOB_DESC_1` VARCHAR(64),
		`JOB_DESC_2` VARCHAR(64),
		`JOB_DESC_3` VARCHAR(64),
		`JOB_DESC_4` VARCHAR(64),
		`JOB_DESC_5` VARCHAR(64),
		`PRJ_DESC` VARCHAR(64) NOT NULL,                                                             
		`CLIENT` VARCHAR(64) NOT NULL,					
		`URL` VARCHAR(64) NOT NULL,	
		PRIMARY KEY  (`JSKR_PROJECTS_ID`),
		CONSTRAINT `FKJSKR6` FOREIGN KEY (`JSK_ID`) REFERENCES `JSK_PRFL_DTL` (`JSK_ID`)
		)

 
CREATE TABLE `EMPLR_PRFL_DTL` (                           
		 `EMPLR_ID` BIGINT(10) NOT NULL AUTO_INCREMENT,
		 `EMAIL` VARCHAR(50) DEFAULT NULL,            
		 `COMP_NAME` VARCHAR(124) NOT NULL,              
		 `COMP_TYPE` VARCHAR(3) DEFAULT NULL,              
		 `COMP_SIZE` INT(6) DEFAULT NULL,              
		 `FUNCTIONAL_AREA` VARCHAR(32) DEFAULT NULL,  
		 `CORP_ADD` VARCHAR(252) DEFAULT NULL,        
		 `COUNTRY` VARCHAR(32) DEFAULT NULL,          
		 `CITY` VARCHAR(250) DEFAULT NULL,            
		 `CONTACT_NUMBER` BIGINT(12) NOT NULL,        
		 `COMPANY_URL` VARCHAR(64) DEFAULT NULL,            
		 `FIRST_NAME` VARCHAR(32) DEFAULT NULL,       
		 `LAST_NAME` VARCHAR(32) DEFAULT NULL,        
		 `CREATE_DATE` DATETIME DEFAULT NULL,               
		 `LAST_UPDATE_DATE` DATETIME DEFAULT NULL,          
		  PRIMARY KEY  (`EMPLR_ID`)
           ) 

CREATE TABLE `EMPLR_LOGIN` ( 
		 `EMPLR_ID` BIGINT(10) NOT NULL,
		 `EMAIL` VARCHAR(50) NOT NULL default'',            
		 `PASSWORD` VARCHAR(12) DEFAULT NULL,
		 `EMPLR_TYPE` VARCHAR(12) DEFAULT NULL,
		  PRIMARY KEY (`EMPLR_ID`),
  	      UNIQUE KEY `EMPLR_ID` (`EMPLR_ID`)
         )          

CREATE TABLE `EMPLR_TERMS_AGREED` (      
		`EMPLR_ID` BIGINT(10) NOT NULL,
		`TERM_AGREED` VARCHAR(12) DEFAULT NULL,
		`NOTIFICATION_SERVICE_EMAILS` char(1) default NULL,     
        `OTHER_PROMOTION_SPECIAL_OFFERS` char(1) default NULL,
        `PRIVACY_POLICY` varchar(50) NOT NULL,
		 PRIMARY KEY  (`EMPLR_ID`),                                
         UNIQUE KEY `EMPLR_ID` (`EMPLR_ID`)                                                                                    
		 )   
		 
		 
CREATE TABLE `emplr_logo` (                
              `EMPLR_ID` bigint(10) NOT NULL,          
              `LOGO_IMAGE` blob,                       
              `COMP_DESC` varchar(2000) DEFAULT NULL,  
              PRIMARY KEY (`EMPLR_ID`),                
              UNIQUE KEY `EMPLR_ID` (`EMPLR_ID`)       
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1     

            --
--CREATE TABLE `EMPLR_LOGO` (               
--             `EMPLR_ID` BIGINT(10) NOT NULL,      
--             `LOGO_IMAGE` BLOB,
--	     	 `COMP_DESC` VARCHAR(1000) DEFAULT NULL,    			 
--             CONSTRAINT `EMPLR3` FOREIGN KEY (`EMPLR_ID`) REFERENCES `EMPLR_PRFL_DTL`(`EMPLR_ID`)
--           )  --

CREATE TABLE `EMPLR_LOC_DTL` (               
            `EMPLR_ID` BIGINT(10) NOT NULL,      
	     	`LOC_ID` INT(4) NOT NULL,
	     	CONSTRAINT `EMPLR4` FOREIGN KEY (`EMPLR_ID`) REFERENCES `EMPLR_PRFL_DTL` (`EMPLR_ID`)
	     )    		   

CREATE TABLE `EMPLR_ADDR_DTL` (               
         `EMPLR_ID` BIGINT(10) NOT NULL,      
	     `LOC_ID` INT(4) NOT NULL ,
	     `CITY_NAME` VARCHAR(128) NOT NULL,
	     `ADDR_ID` INT(6) NOT NULL,
	     `ADDR_STREET1` VARCHAR(50) DEFAULT NULL,  
	     `ADDR_STREET2` VARCHAR(50) DEFAULT NULL,
	     `ZIP_POST_CD` VARCHAR(50) DEFAULT NULL,  
	     `STATE` VARCHAR(64) NOT NULL, 
	     `COUNTRY` VARCHAR(50) DEFAULT NULL,
	     CONSTRAINT `EMPLR5` FOREIGN KEY (`EMPLR_ID`) REFERENCES `EMPLR_PRFL_DTL` (`EMPLR_ID`)
           )
-- CONSTRAINT `EMPLR6` FOREIGN KEY (`LOC_ID`) REFERENCES `EMPLR_LOC_DTL` (`LOC_ID`)  
CREATE TABLE `EMPLR_LOC_CONT_DTL`(               
         `EMPLR_ID` BIGINT(10) NOT NULL,      
	     `LOC_ID` INT(4) NOT NULL ,
	     `FIRST_NAME_1` VARCHAR(32) DEFAULT NULL,       
	     `LAST_NAME_1` VARCHAR(32) DEFAULT NULL,    
	     `EMAIL_1` VARCHAR(50) DEFAULT NULL,
	     `FIRST_NAME_2` VARCHAR(32) DEFAULT NULL,       
	     `LAST_NAME_2` VARCHAR(32) DEFAULT NULL,    
	     `EMAIL_2` VARCHAR(50) DEFAULT NULL,      
	     `CONTACT_NUM` VARCHAR(128) NOT NULL,
	     `MOBILE` VARCHAR(64) NOT NULL,
	     CONSTRAINT `EMPLR7` FOREIGN KEY (`EMPLR_ID`) REFERENCES `EMPLR_PRFL_DTL` (`EMPLR_ID`)
-- 	     CONSTRAINT `EMPLR8` FOREIGN KEY (`EMAIL`) REFERENCES `EMPLR_LOC_DTL` (`LOC_ID`)
           )           
           
CREATE TABLE `EMPLR_JOB_POSTINGS` (                                                                    
		  `JOB_POSTING_ID` BIGINT(10) NOT NULL AUTO_INCREMENT,                                                     
		  `EMPLR_ID` BIGINT(10) NOT NULL,                                                                          
		  `COMP_NAME` VARCHAR(64) DEFAULT NULL,                                                             
		  `JOB_TITLE` VARCHAR(64) DEFAULT NULL,   
		  `JOB_TYPE` VARCHAR(32) DEFAULT NULL,                                                                 
		  `JOB_CD` VARCHAR(32) DEFAULT NULL,                                                                   
		  `JOB_DESIG` VARCHAR(32) DEFAULT NULL,                                                                
		  `EXPERIENCE` DECIMAL(2,0) DEFAULT NULL,                                                              
		  `JOB_DESC` VARCHAR(1000) DEFAULT NULL,                                                               
		  `NUM_POSTS` VARCHAR(32) DEFAULT NULL,                                                                
		  `SALARAY` DECIMAL(4,2) DEFAULT NULL,                                                                 
		  `JOB_LOC1` VARCHAR(32) DEFAULT NULL,                                                                 
		  `JOB_LOC2` VARCHAR(32) DEFAULT NULL,                                                                 
		  `JOB_LOC3` VARCHAR(32) DEFAULT NULL,                                                                 
		  `JOB_LOC4` VARCHAR(32) DEFAULT NULL,                                                                 
		  `JOB_LOC5` VARCHAR(32) DEFAULT NULL,                                                                 
		  `CONT_NAME` VARCHAR(32) DEFAULT NULL,                                                            
		  `CONT_EMAIL` VARCHAR(32) DEFAULT NULL,                                                            
		  `CONT_PH_NUM` VARCHAR(32) DEFAULT NULL,                                                            
		  `CONT_MOBILE` VARCHAR(32) DEFAULT NULL,                                                            
		  `COMP_URL` VARCHAR(64) DEFAULT NULL,                                                              
		  `JOB_POSTING_DATE` DATETIME DEFAULT NULL,                                                            
		  PRIMARY KEY  (`JOB_POSTING_ID`),                                                                     
		  CONSTRAINT `EMPLR9` FOREIGN KEY (`EMPLR_ID`) REFERENCES `EMPLR_PRFL_DTL` (`EMPLR_ID`)  
		)                             
CREATE TABLE  `EMPLR_WALKIN_JOB_POSTINGS` (                                                                    
			  `JOB_WALKIN_POST_ID` BIGINT(10) NOT NULL AUTO_INCREMENT,                                                     
			  `EMPLR_ID` BIGINT(10) NOT NULL, 
			  `COMPANY_NAME` VARCHAR(64) DEFAULT NULL,
			  `JOB_TITLE` VARCHAR(64) DEFAULT NULL,					  
			  `JOB_TYPE` VARCHAR(32) DEFAULT NULL,                                                                 
			  `JOB_CD` VARCHAR(32) DEFAULT NULL,                                                                   
			  `JOB_DESIG` VARCHAR(32) DEFAULT NULL,                                                                
			  `EXPERIENCE` DECIMAL(2,0) DEFAULT NULL,                                                              
			  `JOB_DESC` VARCHAR(1000) DEFAULT NULL,                                                               
			  `NUM_POSTS` VARCHAR(32) DEFAULT NULL,                                                                
			  `SALARAY` DECIMAL(6,4) DEFAULT NULL,                                                                 
			  `JOB_LOC1` VARCHAR(32) DEFAULT NULL,                                                                 
			  `JOB_LOC2` VARCHAR(32) DEFAULT NULL,                                                                 
			  `JOB_LOC3` VARCHAR(32) DEFAULT NULL,                                                                 
			  `JOB_LOC4` VARCHAR(32) DEFAULT NULL,                                                                 
			  `JOB_LOC5` VARCHAR(32) DEFAULT NULL,                                                                 
			  `CONTACT_NAME` VARCHAR(32) DEFAULT NULL,                                                             
			  `CONTACT_EMAIL` VARCHAR(32) DEFAULT NULL,                                                            
			  `CONTACT_PH_NO` VARCHAR(32) DEFAULT NULL,                                                            
			  `COMPANY_URL` VARCHAR(64) DEFAULT NULL,                                                              
			  `JOB_POSTING_DATE` DATETIME DEFAULT NULL,
			  `JOB_WALKIN_DATE` DATETIME DEFAULT NULL,  					  
			   PRIMARY KEY  (`JOB_WALKIN_POST_ID`),                                                                     
			  CONSTRAINT `EMPLR10` FOREIGN KEY (`EMPLR_ID`) REFERENCES `EMPLR_PRFL_DTL` (`EMPLR_ID`)
		)                             					
CREATE TABLE `EMPLR_PRFLS_SEARCH_HSTRY` (    
		`EMPLR_ID` BIGINT(10) NOT NULL,  
		`PRFLE_SEARCH_DATE` DATETIME DEFAULT NULL,    
		`SEARCH_CRITERIA` DATETIME DEFAULT NULL,
		`PRFLS_DWNLD` BIGINT(10) NOT NULL,
		`PRFLS_VIEWD` BIGINT(10) NOT NULL,
		CONSTRAINT `EMPLR11` FOREIGN KEY (`EMPLR_ID`) REFERENCES `EMPLR_PRFL_DTL` (`EMPLR_ID`)
		  )
				  
CREATE TABLE `EMPLR_JOB_POST_VIEW_DTL` (
		`JOB_POSTING_ID` BIGINT(10) NOT NULL,      
		`EMPLR_ID` BIGINT(10) NOT NULL,  
		`JSK_ID` BIGINT(10) NOT NULL,
		`JOB_POST_VIEWD_DATE` DATETIME DEFAULT NULL,
		CONSTRAINT `EMPLR122` FOREIGN KEY (`EMPLR_ID`) REFERENCES `EMPLR_PRFL_DTL` (`EMPLR_ID`)
		)                      

		
ALTER TABLE JSK_PRFL_DTL AUTO_INCREMENT = 1000;
ALTER TABLE EMPLR_PRFL_DTL AUTO_INCREMENT = 1000;
