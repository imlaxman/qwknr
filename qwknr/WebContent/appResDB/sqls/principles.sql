
if one side is optional take the primary key from the mandatory side and make it the primary key on the optional side
if both sides are optional you may choose either primary key make it foriegn key in the other entity
if both side are mandatory(this is unusual), you should consider combining the two entities into one entity
one to many relationships: the primary key from the "one" side becomes the foriegh key on the "many" side

2nf: all attributes should completely depend on primary key

3nf: all attributes should directly depndent on primary key 


CREATE TABLE `person_one` (
  `personId` int(11) NOT NULL AUTO_INCREMENT,
  `addressid` int(11) NOT NULL,
  `PERSON_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`personId`),
  UNIQUE KEY `addressid` (`addressid`),
  KEY `FK45713E5C320FE683` (`addressid`),
  CONSTRAINT `FK45713E5C320FE683` FOREIGN KEY (`addressid`) REFERENCES `address` (`addressId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1

CREATE TABLE `address` (
  `addressId` int(11) NOT NULL AUTO_INCREMENT,
  `ADDRESS1` varchar(255) DEFAULT NULL,
  `ADDRESS2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`addressId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1

