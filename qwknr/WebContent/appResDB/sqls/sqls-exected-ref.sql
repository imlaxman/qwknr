

/*execute below query to start sequnce that means it starts from 100*/
ALTER TABLE person_one AUTO_INCREMENT = 100;

/*one to many   one to one */
CREATE TABLE `person_one` (
  `personId` int(11) NOT NULL AUTO_INCREMENT,
  `addressid` int(11) NOT NULL,
  `PERSON_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`personId`),
  UNIQUE KEY `addressid` (`addressid`),
  KEY `FK45713E5C320FE683` (`addressid`),
  CONSTRAINT `FK45713E5C320FE683` FOREIGN KEY (`addressid`) REFERENCES `address` (`addressId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1

CREATE TABLE `address` (
  `addressId` int(11) NOT NULL AUTO_INCREMENT,
  `ADDRESS1` varchar(255) DEFAULT NULL,
  `ADDRESS2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`addressId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1
 



/*one to many   one to one */

CREATE TABLE `employee` (
  `employee_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `birth_date` date DEFAULT NULL,
  `cell_phone` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1

CREATE TABLE `employeedetail` (
  `employee_id` bigint(20) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`employee_id`),
  UNIQUE KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1





/*one to many   one department many employees*/

CREATE TABLE `department_onetomany_set` (
  `DEPARTMENT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DEPT_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DEPARTMENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1

CREATE TABLE `employee_onetomany_set` (
  `employee_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `birth_date` date DEFAULT NULL,
  `cell_phone` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `department_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `FK3BD178D2999BACD5` (`department_id`),
  CONSTRAINT `FK3BD178D2999BACD5` FOREIGN KEY (`department_id`) REFERENCES `department_onetomany_set` (`DEPARTMENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1

=================================================================================================

=================================================================================================
CREATE TABLE `jsk_prfl_dtl9` (
  `JSK_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CERTIFICATIONS` varchar(128) DEFAULT NULL,
  `CURRENT_COMPANY` varchar(32) DEFAULT NULL,
  `CURRENT_SALARY` decimal(7,0) DEFAULT NULL,
  `DESIGNATION` varchar(32) DEFAULT NULL,
  `EDUCATION` varchar(6) DEFAULT NULL,
  `EXPECTED_SALARY` decimal(7,0) DEFAULT NULL,
  `EXPERIENCE` tinyint(4) DEFAULT NULL,
  `FIRST_NAME` varchar(32) DEFAULT NULL,
  `FUNCTIONAL_AREA` varchar(64) DEFAULT NULL,
  `KEY_SKILLS` longtext,
  `LAST_NAME` varchar(32) DEFAULT NULL,
  `LOCATION` varchar(32) DEFAULT NULL,
  `MASTERS_EDUCATION` varchar(6) DEFAULT NULL,
  `MOBILE` varchar(14) DEFAULT NULL,
  `PRFL_CREATE_DATE` datetime DEFAULT NULL,
  `PRFL_LAST_UPDATE_DATE` datetime DEFAULT NULL,
  `RESUME_TITLE` varchar(32) DEFAULT NULL,
  `WILLING_TO_RELOCATE` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`JSK_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1

CREATE TABLE `jsk_login9` (
  `JSK_ID` bigint(20) NOT NULL,
  `EMAIL` varchar(50) NOT NULL,
  `PASSWORD` varchar(12) DEFAULT NULL,
  `USER_TYPE` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`JSK_ID`),
  UNIQUE KEY `JSK_ID` (`JSK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1


CREATE TABLE `jsk_job_applications1` (
  `JOB_APPLICATION_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `JOB_APPLIED_DATE` datetime DEFAULT NULL,
  `JOB_POSTING_ID` bigint(20) NOT NULL,
  `JSK_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`JOB_APPLICATION_ID`),
  UNIQUE KEY `JOB_APPLICATION_ID` (`JOB_APPLICATION_ID`),
  KEY `FK5CFC57AF99EEFDE7` (`JSK_ID`),
  CONSTRAINT `FK5CFC57AF99EEFDE7` FOREIGN KEY (`JSK_ID`) REFERENCES `jsk_prfl_dtl9` (`JSK_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1

