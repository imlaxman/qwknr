var serviceModule = angular.module('sampleService',[]);
/*alert("1");*/
	serviceModule.service('sampleService', function() {
	    return {
	        getUserData: function() {
	            return [
	                { firstName: 'Java', lastName: 'Honk'},
	                { firstName: 'Alan', lastName: 'Lee'},
	                { firstName: 'Tom', lastName: 'Nally'}
	            ];
	        }		    
	    };
	});