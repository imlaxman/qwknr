package JobSeekerEmailSender;

import java.io.UnsupportedEncodingException;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.zsft.app.qwnr.util.MailUtil;
import com.zsft.app.qwnr.util.StringUtils;

public class UserEmailSender {
	
	public void sendNewJobSeekerProfileRegistrationURL(String fromEmail,String password,String toEmail,String url, String name){
		Session session = MailUtil.buildMailSession(fromEmail,password);
		try{
	        String body = "";
	        StringBuffer sbf = new StringBuffer();
	        
	        sbf.append("<div style=\"margin-left:3%;font-size: 14px;\">");
	        sbf.append("<p><span style=\"color: #0066cc\"><strong>Congratulations! "+name+"</strong></span></p>");
	        sbf.append("<p><span style=\"color: #0066cc\">You are 1 step away, Please provide your professional details by click on below button.</span></p>");
	        sbf.append("<p><a href=\""+url+"\" target=\"_blank\"><button style=\"background-color:maroon; padding: 5px; border-radius: 5px; color: white\">Register</button></a></p>");
	        sbf.append("<p><span style=\"color: #0066cc\">If the above link is not working, Copy and Paste the below link into your browser.</span></p>");
	        sbf.append("<p><span><a href=\"http://jumbojobs.com/\" target=\"_blank\">"+url+"</a></span></p>");
	        sbf.append("</div>");

	        body = sbf.toString();
	        System.out.println("URL: "+url);
	        MimeBodyPart textPart = new MimeBodyPart();
	       // textPart.setHeader("Content-Type", "text/html; charset=\"utf-8\"");
	        textPart.setContent(body, "text/html; charset=utf-8");
	        MimeMessage message = new MimeMessage(session);
	        //message.setFrom(new InternetAddress(fromEmail));
	        message.setFrom(new InternetAddress(fromEmail,"Jumbo Jobs"));
	        message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
	        message.setSubject("Jumbojobs Profile Reqistration");
	        MimeMultipart multipart = new MimeMultipart("mixed");
	        multipart.addBodyPart(textPart);
	        message.setContent(multipart);
	        Transport.send(message);
			  } catch (AddressException e) {
				e.printStackTrace();
			} catch (MessagingException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	}
	
	
	
	public void sendJobSeekerRegistrationSuccessEmail(String fromEmail,String password,String toEmail, String name){
		Session session = MailUtil.buildMailSession(fromEmail,password);
	try{
        MimeMessage message = new MimeMessage(session);
        
        message.setFrom(new InternetAddress(fromEmail,"Jumbo Jobs"));
   //     message.setFrom(new InternetAddress(fromEmail),"Jumbo Jobs");
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
        message.setSubject("Your Registration Has Been Successful!");
        String body = "";
        StringBuffer sbf = new StringBuffer();
        
        sbf.append("<table width=\"50%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"");
        sbf.append("style=\"margin:auto; padding: 0; background-color: #f1f1f2; text-align:center;\">");
        sbf.append("<tbody>");
        sbf.append("<tr>");
        sbf.append("<td>");
        sbf.append("");
        sbf.append("<table width=\"600\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"");
        sbf.append("style=\"margin: 35px auto; padding: 0; border-spacing: 0; font-family: Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif; font-size: 15px; color: #333333; background-color: #ffffff\">");
        sbf.append("<tbody>");
        sbf.append("<tr>");
        sbf.append("<td style=\"padding: 15px 30px 15px 30px\">");
        sbf.append("<h1 style=\"margin-top: 13px; text-align: center; font-weight: 300\">Registration Successful</h1>");
        sbf.append("");
        sbf.append("<hr");
        sbf.append("style=\"border-style: solid; border-color: #d3d4d5; margin: 25px 0\">");
        sbf.append("");
        sbf.append("<p>Hi "+name+",</p>");
        sbf.append("");
        sbf.append("<p>Congratulations! Your Registration has been successful !</p>");
        sbf.append("");
        sbf.append("<p>This is your confirmation that you have registered for a new");
        sbf.append("account at <a href=\"http://jumbojobs.com/\" target=\"_blank\">www.jumbojobs.com</a>");
        sbf.append("with the login ID of <a href=\"mailto:abc@abc.com\"");
        sbf.append("target=\"_blank\">"+toEmail+"</a>");
        sbf.append("</p>");
        sbf.append("</td>");
        sbf.append("</tr>");
        sbf.append("</tbody>");
        sbf.append("</table>");
        sbf.append("");
        sbf.append("</td>");
        sbf.append("</tr>");
        sbf.append("</tbody>");
        sbf.append("</table>");

        body = sbf.toString();
        MimeBodyPart textPart = new MimeBodyPart();
        textPart.setHeader("Content-Type", "text/html; charset=\"utf-8\"");
        textPart.setContent(body, "text/html; charset=utf-8");
        MimeMultipart multipart = new MimeMultipart("mixed");
        multipart.addBodyPart(textPart);
        message.setContent(multipart);
        Transport.send(message);
		  } catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void sendForgotPasswordUrlLink(String fromEmail,String password,String toEmail,String url){
		Session session = MailUtil.buildMailSession(fromEmail,password);
	try{
		String  token = StringUtils.generateRandomString();
		
        String body = "";  
        StringBuffer sbf = new StringBuffer();
        
        sbf.append("<div style=\"margin-left:3%;font-size:14px;\">");
        sbf.append("<p><span style=\"color: #0066cc\"><strong>We have received request to reset your password.</strong></span></p>");
        sbf.append("<p><span style=\"color: #0066cc\">If you want to reset your password, click on the below button.</span></p>");
        sbf.append("<p><a href=\"" +url+"\" "
        		+ "target=\"_blank\"><button style=\"background-color:green; padding: 5px; border-radius: 5px; color: white\">Reset Password</button></a></p>");
        sbf.append("<p><span style=\"color: #0066cc\">If the above link is not working, Copy and Paste the below link into your browser.</span></p>");
        sbf.append("<p><span><a href=\"http://jumbojobs.com/\" target=\"_blank\">"
        		+ " "+url+"  </a></span></p>");
        sbf.append("<p><span style=\"color: #0066cc\">If you are not requested for resetting the password, ignore this email.</span></p>");
        sbf.append("</div>");
        sbf.append("");

        
        body = sbf.toString();
        MimeBodyPart textPart = new MimeBodyPart();
        textPart.setHeader("Content-Type", "text/html; charset=\"utf-8\"");
        textPart.setContent(body, "text/html; charset=utf-8");
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(fromEmail));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail.trim()));
        message.setSubject("Password change request has been received");
        
        MimeMultipart multipart = new MimeMultipart("mixed");
        multipart.addBodyPart(textPart);
        message.setContent(multipart);
        // Send message
        Transport.send(message);
		  } catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}


}
