package com.zsft.app.qwnr.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.zsft.app.qwnr.dao.AdminDaoImpl;
import com.zsft.app.qwnr.model.beans.AdminUserLogin;
import com.zsft.app.qwnr.model.beans.AdminUserReg;
import com.zsft.app.qwnr.model.beans.Category;
import com.zsft.app.qwnr.model.beans.ContactForm;
import com.zsft.app.qwnr.model.beans.Level;
import com.zsft.app.qwnr.model.beans.NewsLetter;
import com.zsft.app.qwnr.model.beans.Questionnaire;

public class AdminBoImpl implements AdminBo{

	@Autowired
	private AdminDaoImpl adminDao;

	@Override
	public boolean isEmailRegistered(String email) {
		// TODO Auto-generated method stub
		return false;
	}

	public AdminDaoImpl getAdminDao() {
		return adminDao;
	}

	public void setAdminDao(AdminDaoImpl adminDao) {
		this.adminDao = adminDao;
	}

	@Override
	public void saveAdmin(AdminUserReg adminReg) {
		adminDao.saveAdmin(adminReg);
		
	}

	@Override
	public AdminUserLogin adminLogin(String email, String password) {
		return adminDao.adminLogin(email, password);
	}

	@Override
	public int forgotPassword(String email) {
		// TODO Auto-generated method stub
		return adminDao.forgotPassword(email);
	}

	@Override
	public void saveQuestionnaire(Questionnaire questionnaire) {
		adminDao.saveQuestionnaire(questionnaire);
		
	}

	@Override
	public void saveContactForm(ContactForm contactForm) {
		adminDao.saveContactForm(contactForm);
		
	}

	@Override
	public void saveNewsLetter(NewsLetter newsLetter) {
		adminDao.saveNewsLetter(newsLetter);
		
	}

	@Override
	public List<String> getAllLevels() {
		return adminDao.getAllLevels();
	}

	@Override
	public List<String> getAllCategories() {
		return adminDao.getAllCategories();
	}

	@Override
	public int getCategoryID(String categoryName) {
		return adminDao.getCategoryID(categoryName);
	}

	@Override
	public int getLevelID(String levelName) {
	
		return adminDao.getLevelID(levelName);
	}

	@Override
	public void saveCategory(Category category) {
		adminDao.saveCategory(category);
		
	}

	
	
	
	
	
	
	
	
}
