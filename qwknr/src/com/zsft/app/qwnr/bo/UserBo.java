package com.zsft.app.qwnr.bo;

import java.util.List;

import com.zsft.app.qwnr.model.beans.NewsLetter;
import com.zsft.app.qwnr.model.beans.Questionnaire;
import com.zsft.app.qwnr.model.beans.UserLogin;
import com.zsft.app.qwnr.model.beans.UserQuestionnairesCompletion;
import com.zsft.app.qwnr.model.beans.UserReg;
import com.zsft.app.qwnr.model.beans.UserTempReg;

public interface UserBo {

	public boolean isEmailRegistered(String email);
	public void saveUserTempReg(UserTempReg userTempReg);
	public void saveUser(UserReg userReg);
	public UserLogin userLogin(String userName, String password);
	public int forgotPassword(String email);
	public UserTempReg getUserTempRegistration(long regid,String email,String token);
	public void saveNewsLetter(NewsLetter newsLetter);
	public void saveUserQuestionnaireAnswer(UserQuestionnairesCompletion userQuestionnairesCompletion);
	public Questionnaire getQuestionnaire(int questionId);
	public List<Questionnaire> getEmployees(int categoryId);
	public List<Questionnaire> getEmployees();
	public List<Questionnaire> getRandomQuestionnaires(String category,int questionId,String randomNumbers);
}
