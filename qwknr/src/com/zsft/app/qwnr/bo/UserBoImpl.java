package com.zsft.app.qwnr.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.zsft.app.qwnr.dao.UserDaoImpl;
import com.zsft.app.qwnr.model.beans.NewsLetter;
import com.zsft.app.qwnr.model.beans.Questionnaire;
import com.zsft.app.qwnr.model.beans.UserLogin;
import com.zsft.app.qwnr.model.beans.UserQuestionnairesCompletion;
import com.zsft.app.qwnr.model.beans.UserReg;
import com.zsft.app.qwnr.model.beans.UserTempReg;

public class UserBoImpl implements UserBo {

	@Autowired
	private UserDaoImpl userDao;
	
	public UserDaoImpl getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDaoImpl userDao) {
		this.userDao = userDao;
	}

	@Override
	public boolean isEmailRegistered(String email) {
		return userDao.isEmailRegistered(email);
	}

	@Override
	public void saveUser(UserReg userReg) {
		userDao.saveUser(userReg);		
	}

	@Override
	public UserLogin userLogin(String email, String password) {
		return userDao.userLogin(email, password);
	}

	@Override
	public int forgotPassword(String email) {
		//userDao.getForgotPasswordRequestsObj(email, token)
		return 0;
	}

	@Override
	public void saveUserTempReg(UserTempReg userTempReg) {
		userDao.saveUserTempReg(userTempReg);
		
	}

	@Override
	public UserTempReg getUserTempRegistration(long regid,String email, String token) {
		return userDao.getJobSeekerTemporaryRegistration(regid, email, token);
	}

	@Override
	public void saveUserQuestionnaireAnswer(
			UserQuestionnairesCompletion userQuestionnairesCompletion) {
		userDao.saveUserQuestionnaireAnswer(userQuestionnairesCompletion);		
	}

	@Override
	public Questionnaire getQuestionnaire(int questionId) {
		
		return userDao.getQuestionnaire(questionId);
	}
	
	public List<Questionnaire> getRandomQuestionnaires(String category,int questionId,String randomNumbers) {
		return userDao.getRandomQuestionnaires(category, questionId, randomNumbers);
	}

	@Override
	public List<Questionnaire> getEmployees() {
		return userDao.getEmployees();
	}

	@Override
	public void saveNewsLetter(NewsLetter newsLetter) {
		userDao.saveNewsLetter(newsLetter);
		
	}

	@Override
	public List<Questionnaire> getEmployees(int categoryId) {
		
		return userDao.getEmployees(categoryId);
	}
	
	
}
