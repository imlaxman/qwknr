package com.zsft.app.qwnr.constants;

public interface CommonConstants {

	public static final String USER_TYPE = "userType";
	public static final String USER_TYPE_JSKR = "JOBSEEKAR";
	public static final String USER_TYPE_EMPLOYER = "EMPLOYER";
	public static final String USER_TYPE_ADMIN = "ADMIN";
	
	public static final String SESSION_ID = "SESSION_ID";
	public static final String JSKR_SESSION = "JSKR_SESSION";
	public static final String EMPLR_SESSION = "EMPLR_SESSION";
	public static final String ADMIN_SESSION = "ADMIN_SESSION";
	public static final String QUESTIONNAIRE_SESSION ="QUESTIONNAIRE_SESSION";
	public static final String SESSION_USER_NAME = "SESSION_USER_NAME";
	
	public static final String DEVELOPMENT_ENVIRONMENT = "DEV";
	public static final String TEST_ENVIRONMENT = "QA";
	public static final String PRODUCTION_ENVIRONMENT = "PRD";
	
	
	public static final String CV_REPORT_QERY_INDICATOR_USER = "USER";
	public static final String CV_REPORT_QERY_INDICATOR_EMPLOYER = "EMPLOYER";
	
	
	public static final String ONE_DAY_OLD = "1 day old";
	public static final String THREE_DAYS_OLD = "3 days old";
	public static final String SEVEN_DAYS_OLD = "7 days old";
	
	public static final String ALL_JOBS = "All Jobs";
	
	/*public static final String COMPANY = "Company";
	public static final String PLACEMENT_AGENCY = "Job Placement Agency";*/
	public static final String COMPANY = "C";
	public static final String PLACEMENT_AGENCY = "P";
	
	
	/*public static final String EMPLR_TYPE_GENERAL="G"; //CAN CREATE A TOTAL OF 10 ACCOUNTS INCLUDING ADMING USER TYPE
	public static final String EMPLR_TYPE_FEATURED="F"; //CAN CREATE A TOTAL OF 20 ACCOUNTS INCLUDING ADMING USER TYPE
	public static final String EMPLR_TYPE_GOLD="G"; //CAN CREATE A TOTAL OF 40 ACCOUNTS INCLUDING ADMING USER TYPE
	public static final String EMPLR_TYPE_PLATINUM="P"; //CAN CREATE A TOTAL OF 100 ACCOUNTS INCLUDING ADMING USER TYPE
*/	
	//public static final String EMPLR_ACC_TYPE_GENERAL = "G";
	//public static final String EMPLR_ACC_TYPE_FEATURED = "F";
	//public static final String EMPLR_ACC_TYPE_GOLD = "P";
	
	public static final String EMPLR_USER_TYPE_PARENT = "P";
	public static final String EMPLR_USER_TYPE_SUB_USER = "S";
	
	public static final String JOB_TYPE_WALKIN="W";
	public static final String JOB_TYPE_JOB="J";
	
	
	public static final String COMPANIES = "Companies";
	public static final String CONSULTANCY = "Consultancy";
	public static final String CONSULTANCIES = "Consultancies";
	public static final String FRESHER = "Fresher";
	
	/*public static final String ZERO_TWO_LAKHS = "0-2";
	public static final String TWO_FIVE_LAKHS = "2-5";
	public static final String FIVE_EIGHT_LAKHS = "5-8";
	public static final String EIGHT_TEN_LAKHS = "8-10";
	public static final String TEN_FIFTEEN_LAKHS = "10-15";
	public static final String FIFTEEN_TWENTY_LAKHS = "15-20";
	public static final String TWENTY_TWENTYFIVE_LAKHS = "20-25";
	public static final String TWENTYFIVE_THIRTY_LAKHS = "25-30";
	
	public static final String THIRTY_THIRTYFIVE_LAKHS = "30-35";
	public static final String THIRTYFIVE_FOURTY_LAKHS = "35-40";
	public static final String FOURTY_FOURTYFIVE_LAKHS = "40-45";
	public static final String FOURTYFIVE_FIFTY_LAKHS = "45-50";
	public static final String FIFTY_PLUS_LAKHS = "50+";
	
	public static final String FRESHNESS_FILTER_REQUEST="freshnessFlterReq";
	public static final String COMPANY_TYPE_REQUEST="companyTypeFlterReq";
	public static final String SALARY_FILTER_REQUEST="salaryFlterReq";
	public static final String COMPANY_FILTER_REQUEST="companyFlterReq";
	public static final String LOCATION_FILTER_REQUEST="locationFlterReq";
	public static final String JOB_TITLE_FILTER_REQUEST="jobTitleFlterReq";
	public static final String JOB_TYPE_REQUEST="jobTypeFlterReq";
	
	public static final String KEY_SKILLS_FILTER_REQUEST="keySkillsFilterReq";
	public static final String RESUME_TITLE_REQUEST="resumeTypeFilterReq";*/
	
	
	
	public static final String SUBMIT_ACTION="ACTION";
	
	public static final String ACTIVE_JOB_STATUS="A";
	
	public static final String JOB_STATUS_ACTIVE="A";
	public static final String JOB_STATUS_INACTIVE="N";
	public static final String JOB_STATUS_PASS="P";
	
	public static final String JOB_ID = "jobId";
	
	
	
}
