package com.zsft.app.qwnr.constants;

public interface FormConstants {
	public static String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static String STRING_PATTERN = "[a-zA-Z]+";
	public static String NUMBER_PATTERN = "[0-9]+";
	public static String MOBILE_PATTERN = "[0-9]{10}";  
	
	//public static String DATE_PATTERN = "^[0-3]?[0-9]/[0-3]?[0-9]/(?:[0-9]{2})?[0-9]{2}$"; //01/01/2011
	public static String DATE_PATTERN = "^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$"; //01/01/2011
	
	
	public static String email = "Email";
	public static String password = "Password";
	public static String FirstName = "First Name";
	public static String LastName = "Last Name";
	public static String DateOfBirth = "Date Of Birth";
	public static String CV_Title = "CV Title";
	public static String Key_Skills = "Key Skills";
	public static String Gender = "--- Select Gender---";
	public static String Nationality = "Nationality";
	public static String Current_Location = "Current Location";
	public static String Mobile = "Mobile";
	public static String Experience_Yrs = "--- Experience Yr's ---";
	public static String Qualification = "Qualification";
	public static String Higest_Qualification = "Higest Qualification";
	public static String Functional_Area = "Functional Area";
	public static String Industry_type = "Industry type";
	
	public static String Active_Job_Status = "A";
	
	
	/*public static String FirstName = "First";
	public static String FirstName = "First";
	public static String FirstName = "First";
	public static String FirstName = "First";
	public static String FirstName = "First";
	public static String FirstName = "First";
	public static String FirstName = "First";
	public static String FirstName = "First";
	public static String FirstName = "First";
	public static String FirstName = "First";*/
}
