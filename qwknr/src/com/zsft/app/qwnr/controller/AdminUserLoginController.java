package com.zsft.app.qwnr.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zsft.app.qwnr.bo.AdminBoImpl;
import com.zsft.app.qwnr.constants.CommonConstants;
import com.zsft.app.qwnr.model.beans.AdminUserLogin;
import com.zsft.app.qwnr.model.beans.AdminUserReg;
import com.zsft.app.qwnr.ui.beans.FormBean;
import com.zsft.app.qwnr.util.AdminSession;
import com.zsft.app.qwnr.util.BusinessUtil;

@Controller
public class AdminUserLoginController {
	private static Logger log = Logger.getLogger(AdminUserLoginController.class);

	@Autowired
    @Qualifier("adminLoginFormValidator")
    private Validator validator;
	
	@InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@Autowired
	private AdminBoImpl adminBo;
	
	
	@RequestMapping(value = { "/admin-user-login" },method = RequestMethod.POST)
	public String submitForm(Model model, @Validated FormBean formBean, BindingResult result,HttpServletRequest request,Errors errors) {
		//log.info("START : Job seeker: "+formBean.getLoginEmail()+"_"+formBean.Password());
		//log.info("START : Job seeker: "+formBean.getLoginEmail()+"_"+formBean.Password());
		//log.info("START : Job seeker: "+formBean.getLoginEmail()+"_"+formBean.Password());
		AdminUserLogin adminUserLogin=null;
		model.addAttribute("formBean", formBean);
		
		if(result.hasErrors()) {
			return "admin-login";
		}
		adminUserLogin = adminBo.adminLogin(formBean.getLoginEmail().trim(), BusinessUtil.md5CodeGen(formBean.getLoginPassword().trim()));
		if (adminUserLogin == null) {
			result.rejectValue("loginEmail", "valid.loginerror");
			return "admin-login";
		}

		AdminUserReg adminUserReg = adminUserLogin.getAdminUserReg();
		AdminSession adminSession = new AdminSession();
		adminSession.setAdminEmail(adminUserLogin.getLoginEmail());
		adminSession.setAdminId(formBean.getAdminUserId());
		//adminSession.setAdminType(formBean.getFirstName());
		adminSession.setContactNumber(formBean.getMobile());
		adminSession.setFirstName(formBean.getFirstName());
		adminSession.setLastName(formBean.getLastName());
		adminSession.setLastUpdateDate(new Date());
		
		HttpSession session =request.getSession();
		String sessionId = session.getId();
		//session.setAttribute(CommonConstants.SESSION_ID, sessionId);
		session.setAttribute(CommonConstants.USER_TYPE, CommonConstants.USER_TYPE_JSKR);
		session.setAttribute(CommonConstants.USER_TYPE_JSKR, CommonConstants.USER_TYPE_JSKR);
		session.setAttribute(CommonConstants.JSKR_SESSION, adminSession);
		session.setAttribute(CommonConstants.SESSION_USER_NAME, adminUserReg.getFirstName()+" "+adminUserReg.getLastName());
		
		System.out.println("**** SETTING INTO SESSION: "+adminUserReg.getFirstName() +"   NAME "+adminUserReg.getFirstName());
		System.out.println("**** SESSION ID: "+sessionId);
		//session.setAttribute(CommonConstants.JSKR_SESSION, adminSession); 
		
		return "redirect:admindashboard";
	}
	
	
}
