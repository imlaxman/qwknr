package com.zsft.app.qwnr.controller;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zsft.app.qwnr.bo.AdminBoImpl;
import com.zsft.app.qwnr.model.beans.AdminUserLogin;
import com.zsft.app.qwnr.model.beans.AdminUserReg;
import com.zsft.app.qwnr.ui.beans.FormBean;
import com.zsft.app.qwnr.util.BusinessUtil;
import com.zsft.app.qwnr.util.PropertyUtil;
import com.zsft.app.qwnr.util.StringUtils;

@Controller
public class AdminUserRegistrationController {
	private static Logger log = Logger.getLogger(AdminUserRegistrationController.class);

	@Autowired
    @Qualifier("adminRegistrationFormValidator")
    private Validator validator;
	
	@InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@Autowired
	private AdminBoImpl adminBo;
	
	@RequestMapping(value = { "/admin-reg" },method = RequestMethod.POST)
	public String submitForm(Model model, @Validated FormBean formBean, BindingResult result) {
		model.addAttribute("formBean", formBean);
		if(result.hasErrors()) {
			return "job-seeker-login-registration";
		} 
			AdminUserReg adminUserReg = new AdminUserReg();
			AdminUserLogin adminUserLogin = new AdminUserLogin();
			
			//AdminUserLogin
			
			adminUserReg.setFirstName(formBean.getFirstName());
			adminUserReg.setLastName(formBean.getLastName());
			adminUserReg.setMobile(formBean.getMobile());
			adminUserReg.setAdminUserLogin(adminUserLogin);
			adminUserLogin.setAdminUserReg(adminUserReg);
			
			
			adminUserLogin.setLastLoginDate(new Date());
			adminUserLogin.setLoginEmail(formBean.getLoginEmail());
			adminUserLogin.setPassword(BusinessUtil.md5CodeGen(formBean.getLoginPassword().trim()));
			
			adminUserReg.setCreationDate(new Date());
			
			adminBo.saveAdmin(adminUserReg);
			model.addAttribute("message", "Congrats! Your Registration has been successfull, an email has been sent to your registered email please click on link and completed your registration.");
			model.addAttribute("formBean", formBean);
			//return "redirect:registration1-success";
			return "registration1-success";
	}

	
}
