package com.zsft.app.qwnr.controller;


import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zsft.app.qwnr.ui.beans.FormBean;
import com.zsft.app.qwnr.ui.beans.QuestionnaireFormBean;


@Controller
public class HomeController {
	private static Logger log = Logger.getLogger(HomeController.class);
	
	@RequestMapping(value={"/"},method=RequestMethod.GET)
	public String showHomePage(ModelMap model){
		return "/index";
	}
	
	
	@RequestMapping(value={"/header"},method=RequestMethod.GET)
	public String showHeaderPage(ModelMap model){
		return "/header";
	}
	
	
	
	@RequestMapping(value={"/incorrect"},method=RequestMethod.GET)
	public String showRandom1Page(ModelMap model){
		return "/incorrect";
	}
	
	@RequestMapping(value={"/answer"},method=RequestMethod.GET)
	public String showAnswerPage(ModelMap model){
		return "/answer";
	}
	
	
	@RequestMapping(value={"/admindashboard"},method=RequestMethod.GET)
	public String showAdmindashboardPage(ModelMap model){
		return "/admindashboard";
	}
	
	@RequestMapping(value={"/add-category"},method=RequestMethod.GET)
	public String showAddcategoryPage(ModelMap model){
		return "/add-category";
	}
	
	
	@RequestMapping(value={"/footer"},method=RequestMethod.GET)
	public String showFooterPage(ModelMap model){
		return "/footer";
	}

	@RequestMapping(value = { "/adminxx" }, method = RequestMethod.GET)
	public String showadminHomePage(ModelMap model) {
		FormBean formBean = new FormBean();
		model.addAttribute("formBean", formBean);
		return "adminxx";
	}
	
	@RequestMapping(value = { "/admin-reg" }, method = RequestMethod.GET)
	public String showadminReg(ModelMap model) {
		FormBean formBean = new FormBean();
		model.addAttribute("formBean", formBean);
		return "admin-reg";
	}
	@RequestMapping(value = { "/admin-login" }, method = RequestMethod.GET)
	public String showadminLogin(ModelMap model) {
		FormBean formBean = new FormBean();
		model.addAttribute("formBean", formBean);
		return "admin-login";
	}
	@RequestMapping(value = { "/user-reg" }, method = RequestMethod.GET)
	public String showuserReg(ModelMap model) {
		FormBean formBean = new FormBean();
		model.addAttribute("formBean", formBean);
		return "user-reg";
	}
	@RequestMapping(value = { "/user-login" }, method = RequestMethod.GET)
	public String showadminuserLogin(ModelMap model) {
		FormBean formBean = new FormBean();
		model.addAttribute("formBean", formBean);
		return "user-login";
	}
	@RequestMapping(value = { "/home" }, method = RequestMethod.GET)
	public String showhomePage(ModelMap model) {
		FormBean formBean = new FormBean();
		model.addAttribute("formBean", formBean);
		return "home";
	}
	
	
	
	@RequestMapping(value = { "/questionnaire" }, method = RequestMethod.GET)
	public String showQuestionnairePage(ModelMap model) {
		FormBean formBean = new FormBean();
		model.addAttribute("formBean", formBean);
		return "questionnaire";
	}
	
	@RequestMapping(value = { "/question-result" }, method = RequestMethod.GET)
	public String showQuestionResultPage(ModelMap model) {
		FormBean formBean = new FormBean();
		model.addAttribute("formBean", formBean);
		return "question-result";
	}
	
	
	
	@RequestMapping(value = { "/contact" }, method = RequestMethod.GET)
	public String showContactPage(ModelMap model) {
		FormBean formBean = new FormBean();
		model.addAttribute("formBean", formBean);
		return "contact";
	}
	
	@RequestMapping(value = { "/about-us" }, method = RequestMethod.GET)
	public String showAboutUsPage(ModelMap model) {
		FormBean formBean = new FormBean();
		model.addAttribute("formBean", formBean);
		return "about-us";
	}
	
	
}
