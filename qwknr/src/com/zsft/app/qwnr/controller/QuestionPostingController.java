package com.zsft.app.qwnr.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zsft.app.qwnr.bo.AdminBoImpl;
import com.zsft.app.qwnr.model.beans.Questionnaire;
import com.zsft.app.qwnr.ui.beans.FormBean;
import com.zsft.app.qwnr.ui.beans.QuestionnaireFormBean;
import com.zsft.app.qwnr.util.FormBeanUtil;

@Controller
public class QuestionPostingController {
	private static Logger log = Logger.getLogger(QuestionPostingController.class);

	@Autowired
    @Qualifier("questionnairePostingFormValidator")
    private Validator validator;
	
	@InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@Autowired
	private AdminBoImpl adminBo;
	
	
	@RequestMapping(value = { "/create-questionnaire" }, method = RequestMethod.GET)
	public String createNeewQuestion(ModelMap model) {
		/*FormBean formBean = new FormBean();*/
		QuestionnaireFormBean questionnaireFormBean = new QuestionnaireFormBean();
		/*model.addAttribute("formBean", formBean);*/
		
		//GET LEVELS AND CATEGORIES HERE 
		List<String> levelsList= adminBo.getAllLevels();
		List<String> categoriesList= adminBo.getAllCategories();
		List<String> answersList = new ArrayList<String>();
		answersList =Arrays.asList(FormBeanUtil.ANSWERS_OPTIONS);
		model.put("answersList", answersList);
		
		model.addAttribute("levelsList", levelsList);
		model.addAttribute("categoriesList", categoriesList);
		model.addAttribute("questionnaireFormBean", questionnaireFormBean);
		return "create-questionnaire";
	}
	
	
	
	
	@RequestMapping(value = { "/questionnaire-posting-success" }, method = RequestMethod.GET)
	public String showQuestionnaireSuccessPage(ModelMap model) {
		/*FormBean formBean = new FormBean();*/
		QuestionnaireFormBean questionnaireFormBean = new QuestionnaireFormBean();
		/*model.addAttribute("formBean", formBean);*/
		model.addAttribute("questionnaireFormBean", questionnaireFormBean);
		return "questionnaire-posting-success";
	}
	
	
	
	
	@RequestMapping(value = { "/create-questionnaire" },method = RequestMethod.POST)
	public String submitForm(Model model, @Validated QuestionnaireFormBean questionnaireFormBean, BindingResult result) {
		model.addAttribute("questionnaireFormBean", questionnaireFormBean);
		if(result.hasErrors()) {
			return "create-questionnaire";
		} 
			Questionnaire questionnaire = new Questionnaire(); 
			
			questionnaire.setOption1(questionnaireFormBean.getOption1());
			questionnaire.setOption2(questionnaireFormBean.getOption2());
			questionnaire.setOption3(questionnaireFormBean.getOption3());
			questionnaire.setOption4(questionnaireFormBean.getOption4());
			questionnaire.setQuestion(questionnaireFormBean.getQuestion());
			questionnaire.setAnswer(questionnaireFormBean.getAnswer());
			questionnaire.setCreatedBy(1);
			questionnaire.setCreatedDate(new Date());
			questionnaire.setLevelId(adminBo.getLevelID(questionnaireFormBean.getQuestionLevelName()));
			questionnaire.setCategoryId(adminBo.getCategoryID(questionnaireFormBean.getQuestionCategory()));
			/*questionnaire.setLevel(questionnaireFormBean.getQuestionLevelName());
			questionnaire.setTopic(questionnaireFormBean.getQuestionCategory());*/
			
			/*Level level = new Level();
			Topic topic = new Topic();
			
			
			level.setLevelName("1");
			level.setLevelDescription("DESC1");
			
			topic.setTopicName("tp1");
			topic.setTopicDescription("tpdesc1");
			*/
			
			//questionnaire.setLevel(level);
			//questionnaire.setTopic(topic);
			
			
			
			/*questionnaire.setQuestionId(questionnaireFormBean.get);
			
		*/
		
			adminBo.saveQuestionnaire(questionnaire);
			/*model.addAttribute("message", "Questionnaire created successfully!");*/
			model.addAttribute("questionnaireFormBean", questionnaireFormBean);
			//return "redirect:registration1-success";
			return "success";
	}

	/*@RequestMapping(value="/employees", method = RequestMethod.GET)
	public ModelAndView listEmployees() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("employees",  prepareListofBean(adminBo.listEmployeess()));
		return new ModelAndView("employeesList", model);
	}*/
	
	
	
	
}
