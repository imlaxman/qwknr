package com.zsft.app.qwnr.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zsft.app.qwnr.bo.UserBoImpl;
import com.zsft.app.qwnr.constants.CommonConstants;
import com.zsft.app.qwnr.model.beans.UserLogin;
import com.zsft.app.qwnr.model.beans.UserReg;
import com.zsft.app.qwnr.ui.beans.FormBean;
import com.zsft.app.qwnr.util.BusinessUtil;
import com.zsft.app.qwnr.util.UserSession;

@Controller
public class UserLoginController {
	private static Logger log = Logger.getLogger(UserLoginController.class);
	
	@Autowired
    @Qualifier("userLoginFormValidator")
    private Validator validator;
	
	@InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@Autowired
	private UserBoImpl userBo;
	
	
	@RequestMapping(value = { "/user-login" },method = RequestMethod.POST)
	public String submitForm(Model model, @Validated FormBean formBean, BindingResult result,HttpServletRequest request,Errors errors) {
		log.info("START : Job seeker: "+formBean.getLoginEmail()+"_"+formBean.getLoginPassword());
		UserLogin userLogin=null;
		model.addAttribute("formBean", formBean);
		
		if(result.hasErrors()) {
			return "user-login";
		}
		userLogin = userBo.userLogin(formBean.getLoginEmail().trim(), BusinessUtil.md5CodeGen(formBean.getLoginPassword().trim()));
		if (userLogin == null) {
			result.rejectValue("loginEmail", "valid.loginerror");
			return "user-login";
		}

		UserReg userReg =  userLogin.getUserReg();
		UserSession userSession = new UserSession();
		userSession.setUserId(userReg.getUserId());
		userSession.setLoginEmail(formBean.getLoginEmail());
		userSession.setFirstName(userReg.getFirstName());
		userSession.setLastName(userReg.getLastName());

		HttpSession session =request.getSession();
		String sessionId = session.getId();
		//session.setAttribute(CommonConstants.SESSION_ID, sessionId);
		session.setAttribute(CommonConstants.USER_TYPE, CommonConstants.USER_TYPE_JSKR);
		session.setAttribute(CommonConstants.USER_TYPE_JSKR, CommonConstants.USER_TYPE_JSKR);
		session.setAttribute(CommonConstants.JSKR_SESSION, userSession);
		session.setAttribute(CommonConstants.SESSION_USER_NAME, userReg.getFirstName()+" "+userReg.getLastName());
		
		
		System.out.println("**** SETTING INTO SESSION: "+userReg.getUserId() +"   NAME "+userReg.getFirstName());
		return "home";
	}
	
}
