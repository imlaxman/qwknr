package com.zsft.app.qwnr.controller;

import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zsft.app.qwnr.bo.UserBoImpl;
import com.zsft.app.qwnr.model.beans.Questionnaire;
import com.zsft.app.qwnr.model.beans.UserQuestionnairesCompletion;
import com.zsft.app.qwnr.ui.beans.QuestionnaireFormBean;
import com.zsft.app.qwnr.util.BusinessUtil;

@Controller
public class UserQuestionnaireController {
	private static Logger log = Logger.getLogger(UserQuestionnaireController.class);

	@Autowired
    @Qualifier("userQuestionnaireFormValidator")
    private Validator validator;
	
	@InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@Autowired
	private UserBoImpl userBo;
	
	@RequestMapping(value = { "/take-questionnaire" },method = RequestMethod.POST)
	public String submitForm(Model model, @Validated QuestionnaireFormBean questionnaireFormBean, BindingResult result) {
		model.addAttribute("questionnaireFormBean", questionnaireFormBean);
		if(result.hasErrors()) {
			return "job-seeker-login-registration";
		} 
		UserQuestionnairesCompletion userQuestionnairesCompletion = new UserQuestionnairesCompletion(); 
		userQuestionnairesCompletion.setQuestionId(questionnaireFormBean.getQuestionId());
			userQuestionnairesCompletion.setUserId(questionnaireFormBean.getUserId());
			userQuestionnairesCompletion.setQuestionScore(questionnaireFormBean.getQuestionScore());
			userQuestionnairesCompletion.setCompletedInSeconds(1);
			
			
			
			userBo.saveUserQuestionnaireAnswer(userQuestionnairesCompletion);
			model.addAttribute("message", "Congrats! Your Registration has been successfull, an email has been sent to your registered email please click on link and completed your registration.");
			model.addAttribute("userQuestionnairesCompletion", userQuestionnairesCompletion);
			//return "redirect:registration1-success";
			return "registration1-success";
	}
	@RequestMapping("/viewemp")
	public ModelAndView viewemp(ModelMap model,HttpServletRequest request,HttpSession session){  
		Boolean showQuestion=Boolean.FALSE;
        List<Questionnaire> list=userBo.getEmployees();
        
        model.addAttribute("showQuestion",showQuestion);
        model.addAttribute("list", list);
        session.setAttribute("list", list);
        return new ModelAndView("empeditform","command",model);  
    }  
	
	@RequestMapping("/pagination")
	public ModelAndView pagination(@PathVariable int pagenumber,HttpSession session, HttpServletRequest request,
            HttpServletResponse response,ModelMap model){  
        List<Questionnaire> list=userBo.getEmployees(pagenumber);  
        return new ModelAndView("pagination","list",list);  
    }  
	
	
    /* It displays object data into form for the given id.  
     * The @PathVariable puts URL data into variable.*/  
    @RequestMapping(value="/{id}")  
    public ModelAndView edit(@PathVariable int id,HttpSession session, HttpServletRequest request,
            HttpServletResponse response,ModelMap model ){  
    	Object emp=userBo.getQuestionnaire(id);
    	 //List<Questionnaire> list=userBo.getEmployees();
    	 String greetings =  ((Questionnaire) emp).getQuestion();
	    	int greetings5=((Questionnaire) emp).getQuestionId();	
	       	String greetings1 =  ((Questionnaire) emp).getOption1();
	       	String greetings2 =  ((Questionnaire) emp).getOption2();
	       	String greetings3 =  ((Questionnaire) emp).getOption3();
	       	String greetings4 =  ((Questionnaire) emp).getOption4();
	       	String greetings6=	 ((Questionnaire) emp).getAnswer();
	       	int greetings7=((Questionnaire) emp).getCategoryId();
	    	int greetings8=((Questionnaire) emp).getLevelId();
	    	model.addAttribute("greetings", greetings);
	    	model.addAttribute("greetings5", greetings5);
	    	model.addAttribute("greetings1", greetings1);
	    	model.addAttribute("greetings2", greetings2);
	    	model.addAttribute("greetings3", greetings3);
	    	model.addAttribute("greetings4", greetings4);
	    	model.addAttribute("greetings6", greetings6);
	    	model.addAttribute("greetings7", greetings7);
	    	model.addAttribute("greetings8", greetings8);
	    	Boolean showQuestion=Boolean.TRUE;
	    	model.addAttribute("showQuestion",showQuestion);
	    	//model.addAttribute("list",list);
	    	List<String> lists=(List<String>)session.getAttribute("list");
	    	/*for(int i=0;i<lists.size();i++){
	    	List<Integer> id1=lists.get(i).getQuestionId();
	    	}*/
	    	model.addAttribute("lists",lists);
        return new ModelAndView("empeditform","command",model);  
    }  
    @RequestMapping(value="//answercompare")  
    public ModelAndView compare(HttpServletRequest request,HttpServletResponse response,ModelMap model ){
    	String[] username=request.getParameterValues("option");
    	if (username != null) 
		   {
		      for (int i = 0; i < username.length; i++) 
		      {
		         System.out.println ("<b>"+username[i]+"<b>");
		      }
		   }
		   else System.out.println ("<b>none<b>");
		 HttpSession session=request.getSession();  
		String userid = session.getAttribute("str").toString();
		System.out.print(userid);
		int count=0;
		for(int j=0;j<username.length;j++){
		if(userid.equals(username[j])){
			/*count=count+10;
			//request.setAttribute("count", count);
			Counter1 counter=new Counter1();
			counter.setCounter(count);
			int status=Getdata1.counter1(counter);
			if(status>0)	*/
			System.out.print("answer is correct");
			return new ModelAndView("answer");  
			
		}
		else {System.out.print("incorrect");
		return new ModelAndView("incorrect");  
		
		}
		}
		
    	
		 return null;  
    	
    	
    	
    	
    }
}
