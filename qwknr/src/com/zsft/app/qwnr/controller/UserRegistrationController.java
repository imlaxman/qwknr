package com.zsft.app.qwnr.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import JobSeekerEmailSender.UserEmailSender;

import com.zsft.app.qwnr.bo.UserBoImpl;
import com.zsft.app.qwnr.constants.CommonConstants;
import com.zsft.app.qwnr.model.beans.UserLogin;
import com.zsft.app.qwnr.model.beans.UserReg;
import com.zsft.app.qwnr.model.beans.UserTempReg;
import com.zsft.app.qwnr.ui.beans.FormBean;
import com.zsft.app.qwnr.util.BusinessUtil;
import com.zsft.app.qwnr.util.PropertyUtil;
import com.zsft.app.qwnr.util.StringUtils;

@Controller
public class UserRegistrationController {
	private static Logger log = Logger.getLogger(UserRegistrationController.class);

	@Autowired
    @Qualifier("userRegistrationFormValidator")
    private Validator validator;
	
	@InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@Autowired
	private UserBoImpl userBo;
	
	@RequestMapping(value = { "/user-temp-reg" },method = RequestMethod.POST)
	public String userTempReg(Model model, @Validated FormBean formBean, BindingResult result) {
		model.addAttribute("formBean", formBean);
		if(result.hasErrors()) {
			return "user-reg";
		} 
			UserTempReg userTempReg = new UserTempReg();
			
			userTempReg.setFirstName(formBean.getFirstName());
			userTempReg.setLastName(formBean.getLastName());
			userTempReg.setMobile(formBean.getMobile());
			userTempReg.setLoginEmail(formBean.getLoginEmail());
			userTempReg.setPassword(BusinessUtil.md5CodeGen(formBean.getLoginPassword().trim()));
			userTempReg.setToken(StringUtils.generateRandomString());
			userTempReg.setCreationTime(new Date());
			
			PropertyUtil propertyUtil = new PropertyUtil();
			String url="";
			UserEmailSender userEmailSender = new UserEmailSender();
			System.out.print("before if");
			
			
			if(propertyUtil.getEnvironmentDetails().equalsIgnoreCase(CommonConstants.PRODUCTION_ENVIRONMENT)){
				 //url="http://jumbojobs.com/employer-profile-registration?id="+formBean.getJobSeekerTempRedId()+"&token="+formBean.getTokenString()+"&email="+formBean.getRegEmail();
				System.out.print("after if");
				url="http://localhost:8080/qwknr/user-temp-reg?token="+userTempReg.getToken()+"&email="+userTempReg.getLoginEmail();
				System.out.print("after"+url);
				List<String> fromDatls = propertyUtil.getEmailPassowordsForEmailing();
	    		 if(fromDatls !=null && fromDatls.size()>0)
	    			 userEmailSender.sendNewJobSeekerProfileRegistrationURL(fromDatls.get(0),fromDatls.get(1),formBean.getLoginEmail(),url,
	    					 formBean.getFirstName()+" "+formBean.getLastName());
	    		 //fromemail,password,toemail,url   http://localhost:3030/envSpring/job-seeker-reg
			}else{
				System.out.println("in else");
				 url="http://localhost:8080/qwknr/user-registration?token="+userTempReg.getToken()+"&email="+formBean.getLoginEmail();
				 System.out.println("url is:"+url);
				 url="http://localhost:8080/qwknr/user-registration?token="+userTempReg.getToken()+"&email="+userTempReg.getLoginEmail();
				 System.out.println(url);
				 List<String> fromDatls = propertyUtil.getEmailPassowordsForEmailing();
	    		/* if(fromDatls !=null && fromDatls.size()>0)
	    			 userEmailSender.sendNewJobSeekerProfileRegistrationURL(fromDatls.get(0),fromDatls.get(1),propertyUtil.getToEmailForTesting(),url,
	    					 formBean.getFirstName()+" "+formBean.getLastName());*/
			}
			userBo.saveUserTempReg(userTempReg);
			System.out.print("success");
			model.addAttribute("message", "Congrats! Your Registration has been successfull, an email has been sent to your registered email please click on link to activate your account");
			model.addAttribute("formBean", formBean);
			//return "redirect:registration1-success";
			return "success";
	}
	
	
	
	
	
	@RequestMapping(value = { "/user-registration" },method = RequestMethod.GET)
	public String userReg(Model model,HttpServletRequest request) {
		
		String id="";
		String email="";
		String tokenString="";
				
		id = request.getParameter("id");
		email = request.getParameter("email");
		tokenString = request.getParameter("token");
		System.out.println("*********************"+email);
		System.out.println("*********************"+tokenString);
		
		if(email==null||tokenString==null){
			model.addAttribute("message", "Rquested url is not valid, Please create a new registratin request!");
			return "info";	
		}
		
		UserTempReg userTempReg = null;
		 userTempReg = userBo.getUserTempRegistration(1,email, tokenString);
		if(userTempReg ==null){
			model.addAttribute("message", "Rquested url is not valid, Please create a new registratin request!");
			return "info";	
		}
		
			UserReg userReg = new UserReg();
			UserLogin userLogin = new UserLogin();
			
			userReg.setFirstName(userTempReg.getFirstName());
			userReg.setLastName(userTempReg.getLastName());
			userReg.setMobile(userTempReg.getMobile());
			userReg.setCreationDate(new Date());
			userLogin.setLoginEmail(userTempReg.getLoginEmail());
			userLogin.setPassword(BusinessUtil.md5CodeGen(userTempReg.getPassword().trim()));
			userLogin.setLastLoginDate(new Date());
			
			userLogin.setUserReg(userReg);
			userReg.setUserLogin(userLogin);
			
			FormBean formBean = new FormBean();
			userBo.saveUser(userReg);
			/*model.addAttribute("message", "Your Account has been active now");*/
			model.addAttribute("formBean", formBean);
			return "redirect:home";
	}


}
