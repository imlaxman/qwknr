package com.zsft.app.qwnr.dao;

import java.util.List;

import com.zsft.app.qwnr.model.beans.AdminUserLogin;
import com.zsft.app.qwnr.model.beans.AdminUserReg;
import com.zsft.app.qwnr.model.beans.Category;
import com.zsft.app.qwnr.model.beans.ContactForm;
import com.zsft.app.qwnr.model.beans.Level;
import com.zsft.app.qwnr.model.beans.NewsLetter;
import com.zsft.app.qwnr.model.beans.Questionnaire;

public interface AdminDao {

	public boolean isEmailRegistered(String email);
	public void saveAdmin(AdminUserReg adminReg);
	public AdminUserLogin adminLogin(String userName, String password);
	public int forgotPassword(String email);
	public void saveQuestionnaire(Questionnaire questionnaire);
	
	public void saveContactForm(ContactForm contactForm);
	public void saveNewsLetter(NewsLetter newsLetter);
	
	public List<String> getAllLevels();
	public List<String> getAllCategories();
	public int getCategoryID(String categoryName);
	public int getLevelID(String levelName);
	
}