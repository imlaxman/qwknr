package com.zsft.app.qwnr.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import com.zsft.app.qwnr.model.beans.AdminUserLogin;
import com.zsft.app.qwnr.model.beans.AdminUserReg;
import com.zsft.app.qwnr.model.beans.ContactForm;
import com.zsft.app.qwnr.model.beans.NewsLetter;
import com.zsft.app.qwnr.model.beans.Questionnaire;
import com.zsft.app.qwnr.model.beans.UserLogin;


public class AdminDaoImpl extends BaseDAOImpl implements AdminDao{

	@Override
	@Transactional
	public boolean isEmailRegistered(String email) {
		boolean flag = false;
		List<UserLogin> usersList = null;
		Session session = getHibernateSession();
		String qry = "from AdminUserLogin usr where usr.email='" + email + "'";
		usersList = session.createQuery(qry).list();
		if (usersList != null && usersList.size() > 0)
			return flag = true;
		return flag;
	}

	@Override
	@Transactional
	public void saveAdmin(AdminUserReg adminReg) {
		Session session = getHibernateSession();
		session.beginTransaction();
		if (adminReg != null) {
			AdminUserLogin login = adminReg.getAdminUserLogin();
			session.save(adminReg);
			session.flush();
			session.getTransaction().commit();
		}
	}

	@Override
	@Transactional
	public AdminUserLogin adminLogin(String email, String password) {
		System.out.println("-------------JskrDAOImpl.jobSeekarlogin " + email+ "_" + password);
		AdminUserLogin jskLogin = null;
		Session session = getHibernateSession();
		String qry = "from AdminUserLogin lgn where lgn.loginEmail =:email and lgn.password =:password";
		Query query = session.createQuery(qry);
		query.setParameter("email", email);
		query.setParameter("password", password);
		List<AdminUserLogin> jskrs = query.list();
		System.out.println("&&&&&&& jobSeekarlogin jskrs " + jskrs.size());
		if (jskrs != null && jskrs.size() > 0) {
			jskLogin = jskrs.get(0);
		}
		return jskLogin;
	}

	@Override
	@Transactional
	public int forgotPassword(String email) {
		return 0;
	}

	@Override
	@Transactional
	public void saveQuestionnaire(Questionnaire questionnaire) {
	/*	Session hibernateSession = getHibernateSession();
		hibernateSession.saveOrUpdate(questionnaire);*/
		Session session = getHibernateSession();
		session.beginTransaction();
		if (questionnaire != null) {
			session.save(questionnaire);
			session.flush();
			session.getTransaction().commit();
		}
		
	}

	@Override
	@Transactional
	public void saveContactForm(ContactForm contactForm) {
		Session hibernateSession = getHibernateSession();
		hibernateSession.saveOrUpdate(contactForm);			
	}

	@Override
	@Transactional
	public void saveNewsLetter(NewsLetter newsLetter) {
		Session hibernateSession = getHibernateSession();
		hibernateSession.saveOrUpdate(newsLetter);		
	}

	@Override
	@Transactional
	public List<String> getAllLevels() {
		//List<Level> levelsList = null;
		Session session = getHibernateSession();
		String qry = "select levelName from Level order by  levelId";
		return session.createQuery(qry).list();
	}

	@Override
	@Transactional
	public List<String> getAllCategories() {
		//List<Category> categoriessList = null;
		Session session = getHibernateSession();
		String qry = "select categoryName from Category order by categoryId";
		return session.createQuery(qry).list();
	}

	@Override
	public int getCategoryID(String categoryName) {
		Session session=getHibernateSession();
		String qry="select categoryId from Category where categoryName='" + categoryName + "'";
		return (int) session.createQuery(qry).list().get(0);
	}

	@Override
	public int getLevelID(String levelName) {
		Session session=getHibernateSession();
		String qry="select levelId from Level where levelName='" + levelName + "'";
		return (int) session.createQuery(qry).list().get(0);
	}

}
