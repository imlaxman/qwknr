package com.zsft.app.qwnr.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;


import com.zsft.app.qwnr.model.beans.Questionnaire;

public interface BaseDAO {

	
	public void save(Object object)throws HibernateException;
	public void merge(Object object)throws HibernateException;
	public void save(List items)throws HibernateException;
	public void saveOrUpdate(Object object)throws HibernateException;
	public void commit(Session session,Transaction tx)throws HibernateException;
	
	public int updateQuery(String query) throws HibernateException;
	public int executeUpdate(String hibQuery,List parameters);
	public void delete(Object object);
	
	public Object getObject(Class clazz, Serializable id);
	public List getList(String hqlQuery, List<Object> parameters);
	public List getList(String hqlQuery, Map<String, Object> parameters);
	public List getListByNamedQuery(String queryName, List<Object> parameters);
	public List getListByNamedQuery(String queryName, Map<String, Object> parameters);
	public List find(String query);
	
	public Questionnaire getQuestionnaire(int questionId);
	public List<Questionnaire> getEmployees();
	
	
	
	
	
	
}
