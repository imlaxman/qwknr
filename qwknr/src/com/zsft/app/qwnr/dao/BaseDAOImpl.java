package com.zsft.app.qwnr.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.zsft.app.qwnr.exception.ObjectAccessException;
import com.zsft.app.qwnr.model.beans.Questionnaire;

public class BaseDAOImpl implements BaseDAO {
	private static Logger logger = Logger.getLogger(BaseDAOImpl.class);
	
	@Autowired
	protected SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		System.out.println("%%%%%%%%%%%%%%%%%%%%% BaseDAOImpl:  "+sessionFactory);
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	
	public Session getNewSession() {
		Session session = getSessionFactory().openSession();
		return session;
	}
	
	public Session getCurrentSession() throws Exception {
		Session session = null;
		if(sessionFactory !=null){
			System.out.println("_______getCurrentSession()SESSIONFACTORY STATS: " +sessionFactory.getStatistics());
			session = sessionFactory.getCurrentSession();
			if(session != null)
				return session;
			else
				throw new Exception("EXCEPTION: Exception occured while getting Sessionfactory in getCurrentSession()");
		}
		return session;
	}
	
	public Session getHibernateSession() throws HibernateException{
		Session session = null;//sessionFactory.getCurrentSession();
		try{
		if(session == null){
			if(sessionFactory !=null){
				session = sessionFactory.openSession();
				//session = sessionFactory.getCurrentSession();
				System.out.println("___________SESSION:  " +session);
				System.out.println("___________SESSIONFACTORY STATISTICS:  " +sessionFactory.getStatistics());
				//session = sessionFactory.openSession();
			}else{
				
				
				/*try{
				//create application level redirect for sessionfactory null
				//HttpServletRequest origRequest = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
				//ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
				//context.redirect(context.getRequestContextPath()+NavigationConstants.APPLICATIONERRPR500);
				}catch(IOException e){
					logger.debug("DEBUG: SESSIONFACTORY NULL IN BASEDAOIMPLE^^");
				}*/
			}
			
		}
		}catch(HibernateException e){
			logger.debug("HibernateException: Exception while creating Session: ");
			throw e;
		}
		return session;
	}
	
	public void beginTransaction(){
		Session session = getHibernateSession();
		Transaction tx = session.beginTransaction();
	}
	public void commit(Session session,Transaction tx){
		session.flush();
		session.clear();
		tx.commit();
	}
	
	@Transactional
	public void save(Object object) throws HibernateException{
		try {
		Session session = getHibernateSession();
		if(object!=null)
			session.beginTransaction();
			session.save(object);
		session.flush();
		session.getTransaction().commit();
		}catch (HibernateException e) {
			logger.debug("HibernateException: Exception while saving: "+object.getClass());
			throw e;
		}
	}
	
	@Transactional
	public void merge(Object object) throws HibernateException{
		try {
		Session session = getHibernateSession();
		if(object!=null)
			session.merge(object);
		}catch (HibernateException e) {
			logger.debug("HibernateException: Exception while saving: "+object.getClass());
			throw e;
		}
	}
	
	
	
	@Transactional
	public void saveOrUpdate(Object object) throws HibernateException {
		try {
		Session session = getHibernateSession();
		session.saveOrUpdate(object);
		}catch (HibernateException e) {
			logger.debug("HibernateException: Exception while saveOrUpdate: "+object.getClass());
			throw e;
		}
	}
	
	@Transactional
	public void save(List items)throws HibernateException {
		try {
		Session session = getHibernateSession();
		for (Object object: items) {
			session.save(object);
		}
		}catch (HibernateException e) {
			logger.debug("HibernateException: Exception while saving list of items: ");
			throw e;
		}
	}
	
	@Transactional
	public void delete(Object object)throws HibernateException {
		try {
		Session session = getHibernateSession();
		session.delete(object);
		}catch (HibernateException e) {
			logger.debug("HibernateException: Exception while deleting: "+object);
			throw e;
		}
	}
	
	@Transactional
	public int updateQuery(String query) throws HibernateException {
		Session session = null;
		try {
			session = getHibernateSession();
			Query queryObject = session.createQuery(query);
			int count = queryObject.executeUpdate();
			return count;
		}catch (HibernateException e) {
			logger.debug("HibernateException: Exception while updating query: "+query);
			throw e;
		}
	}
	
	@Transactional
	public int executeUpdate(String hibQuery,List parameters){
		try{
		Session session = getHibernateSession();
		Query query= session.createQuery(hibQuery);
		int index = 0;
		for (Object object: parameters) {
			setObject(query, index++, object);
		}
		return query.executeUpdate();
		}catch(HibernateException e){
			logger.debug("HibernateException: Exception while updating query: "+hibQuery);
			throw e;
		}
	}
	
	
	@Transactional
	public Object getObject(Class clazz, Serializable id) {
		Session session = getHibernateSession();
		return session.get(clazz, id);
	}

	@Override
	public List getList(String hqlQuery, List<Object> parameters) {
		return null;
	}

	@Override
	public List getList(String hqlQuery, Map<String, Object> parameters) {
		return null;
	}

	@Override
	public List getListByNamedQuery(String queryName, List<Object> parameters) {
		return null;
	}

	@Override
	public List getListByNamedQuery(String queryName, Map<String, Object> parameters) {
		return null;
	}

	public List find(String query) throws ObjectAccessException {
		try{
		Session s = getHibernateSession();
		Query qr = s.createQuery(query);
		return qr.list();
		}catch(ObjectAccessException oae){
			logger.debug("ObjectAccessException while fetching");
			throw oae;
			
		}catch(HibernateException e){
			logger.debug("");
			throw e;
		}
	}

	
	private void setObject(Query query, int index, Object object) {
		if (object instanceof Integer) {
			query.setInteger(index, (Integer) object);
		} else if (object instanceof BigInteger) {
			query.setBigInteger(index, (BigInteger) object);
		} else if (object instanceof String) {
			query.setString(index, (String) object);
		} else if (object instanceof Long) {
			query.setLong(index, (Long) object);
		} else if (object instanceof BigDecimal) {
			query.setBigDecimal(index, (BigDecimal) object);
		} else if (object instanceof Boolean) {
			query.setBoolean(index, (Boolean) object);
		} else if (object instanceof Double) {
			query.setDouble(index, (Double) object);
		} else if (object instanceof Float) {
			query.setFloat(index, (Float) object);
		} else if (object instanceof Date) {
			query.setDate(index, (Date) object);
		} else if (object instanceof Calendar) {
			query.setCalendar(index, (Calendar) object);
		} else if (object instanceof Short) {
			query.setShort(index, (Short) object);
		} else if (object instanceof Byte) {
			query.setByte(index, (Byte) object);
		} else if (object instanceof Character) {
			query.setCharacter(index, (Character) object);
		}
	}

	

	@Override
	public List<Questionnaire> getEmployees() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Questionnaire getQuestionnaire(int questionId) {
		// TODO Auto-generated method stub
		return null;
	}
}
