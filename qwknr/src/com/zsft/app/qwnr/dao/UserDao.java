package com.zsft.app.qwnr.dao;

import java.util.List;


import com.zsft.app.qwnr.model.beans.ForgotPasswordRequests;
import com.zsft.app.qwnr.model.beans.Questionnaire;
import com.zsft.app.qwnr.model.beans.UserLogin;
import com.zsft.app.qwnr.model.beans.UserQuestionnairesCompletion;
import com.zsft.app.qwnr.model.beans.UserReg;
import com.zsft.app.qwnr.model.beans.UserTempReg;

public interface UserDao {

	public boolean isEmailRegistered(String email);
	public void saveUserTempReg(UserTempReg userTempReg);
	
	public void saveUser(UserReg userReg);
	public UserLogin userLogin(String userName, String password);
	public void createForgotPasswordRequest(ForgotPasswordRequests forgotPasswordRequest);
	public ForgotPasswordRequests getForgotPasswordRequestsObj(String email, String token);
	public UserTempReg getJobSeekerTemporaryRegistration(long regid,String email,String token);
	
	public void saveUserQuestionnaireAnswer(UserQuestionnairesCompletion userQuestionnairesCompletion);
	public Questionnaire getQuestionnaire(int questionId);
	public List<Questionnaire> getEmployees();
	public List<Questionnaire> getEmployees(int pagenumber);
}
