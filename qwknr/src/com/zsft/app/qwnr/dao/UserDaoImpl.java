package com.zsft.app.qwnr.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Transactional;

import com.zsft.app.qwnr.model.beans.ForgotPasswordRequests;
import com.zsft.app.qwnr.model.beans.Questionnaire;
import com.zsft.app.qwnr.model.beans.UserLogin;
import com.zsft.app.qwnr.model.beans.UserQuestionnairesCompletion;
import com.zsft.app.qwnr.model.beans.UserReg;
import com.zsft.app.qwnr.model.beans.UserTempReg;


public class UserDaoImpl extends BaseDAOImpl implements UserDao{

	@Autowired
	SessionFactory sessionFactory;
	  
	
	@Override
	@Transactional
	public boolean isEmailRegistered(String loginEmail) {
		System.out.println("-------------JskrDAOImpl.isEmailRegistered()");
		boolean flag = false;
		List<UserLogin> usersList = null;
		Session session = getHibernateSession();
		String qry = "from UserLogin usr where usr.loginEmail='" + loginEmail + "'";
		usersList = session.createQuery(qry).list();
		if (usersList != null && usersList.size() > 0)
			return flag = true;
		return flag;
	}

	@Override
	@Transactional
	public void saveUserTempReg(UserTempReg userTempReg) {
		Session hibernateSession = getHibernateSession();
		hibernateSession.saveOrUpdate(userTempReg);		
	}
	
	
	
	@Override
	@Transactional
	public void saveUser(UserReg userReg) {
		System.out.println("++++++ in UserDaoImpl addUser Email" + userReg.getUserLogin().getLoginEmail());
		Session session = getHibernateSession();
		session.beginTransaction();
		if (userReg != null) {
			UserLogin login = userReg.getUserLogin();
			// session.save(login);
			// session.save(terms);
			session.save(userReg);
			session.flush();
			session.getTransaction().commit();
		}
	}
	

	/*@Override
	public void saveUser(UserReg userReg) {
		Session hibernateSession = getHibernateSession();
		hibernateSession.save(userReg);		
	}*/

	@Override
	@Transactional
	public UserLogin userLogin(String loginEmail, String password) {
		System.out.println("-------------JskrDAOImpl.jobSeekarlogin " + loginEmail
				+ "_" + password);
		UserLogin userLogin = null;
		Session session = getHibernateSession();
		String qry = "from UserLogin lgn where lgn.loginEmail =:loginEmail and lgn.password =:password";
		Query query = session.createQuery(qry);
		query.setParameter("loginEmail", loginEmail);
		query.setParameter("password", password);
		List<UserLogin> users = query.list();
		System.out.println("&&&&&&& jobSeekarlogin jskrs " + users.size());
		if (users != null && users.size() > 0) {
			userLogin = users.get(0);
		}
		return userLogin;
	}

	@Override
	@Transactional
	public void createForgotPasswordRequest(ForgotPasswordRequests forgotPasswordRequest){
		Session hibernateSession = getHibernateSession();
		hibernateSession.saveOrUpdate(forgotPasswordRequest);
		hibernateSession.flush();
	}
	
	@Override
	@Transactional
	public ForgotPasswordRequests getForgotPasswordRequestsObj(String email, String token) {
		ForgotPasswordRequests forgotPasswordRequests = null;
		Session session = getHibernateSession();
		String qry = "select frgPsdReqObj from ForgotPasswordRequests frgPsdReqObj where frgPsdReqObj.username='"+email+"' and frgPsdReqObj.token='"+token+"'";
		Query query = session.createQuery(qry);
		List li = query.list();
		if(li !=null && li.size()>0)
			return (ForgotPasswordRequests) li.get(0);
		return forgotPasswordRequests;
	}

	@Override
	@Transactional
	public UserTempReg getJobSeekerTemporaryRegistration(long regid,String loginEmail, String token) {

		UserTempReg tempRegg = null;
		Session session = getHibernateSession();
		String qry = "from UserTempReg tempReg where tempReg.loginEmail=:loginEmail and tempReg.token=:token";
		Query query = session.createQuery(qry);
		query.setParameter("loginEmail", loginEmail);
		query.setParameter("token", token);
		List<UserTempReg> regs = query.list();
		if (regs != null && regs.size() > 0)
			tempRegg = regs.get(0);
		return tempRegg;
	}

	@Override
	@Transactional
	public void saveUserQuestionnaireAnswer(
			UserQuestionnairesCompletion userQuestionnairesCompletion) {
		// TODO Auto-generated method stub
		
	}

	
	
	@Override
	@Transactional
	public Questionnaire getQuestionnaire(int idValue) {
		 
		  Session session = getHibernateSession();
		  session.beginTransaction();/*lets hope an id of 1 exists!*/
		  String queryString = "from Questionnaire where questionId = :questionId";
		  Query query = session.createQuery(queryString);
		  query.setInteger("questionId", idValue);
		  Object queryResult = query.uniqueResult();
		  Questionnaire user = (Questionnaire)queryResult;
		  return user;
		 }
	/*public List<Questionnaire> getQuestionnaire(int questionId) {
		@SuppressWarnings("unchecked")
		List query = sessionFactory.openSession().createQuery("from Questionnaire where question_id = :question_id").list();
			       
		       
		return query;
		
		
		System.out.print(questionId);
		Session session = getHibernateSession();
		System.out.print(questionId);
		String sql="from Questionnaire where question_id=?";
		Query query = session.createQuery(sql);
		List<Questionnaire> list=query.list();
		return list;
	//return sessionFactory.getCurrentSession().get(Questionnaire.class,new Object[]{questionId});
		
		
				
		
		Session session = getHibernateSession();
		String sql="from Questionnaire where question_id=?";
		Query query = session.createQuery(sql);
		
		Questionnaire[] emp = (Questionnaire[]) query.uniqueResult();
		
		List results=query.list();
		
		Iterator l=results.iterator();
		while(l.hasNext()){
			Object o=l.next();
			Questionnaire p=(Questionnaire)o;
			System.out.println("Product Name : "+p.getQuestionId());
		}
		return emp;
		
		
	}
	*/
	@SuppressWarnings({ "unchecked" })
	@Override
	public List<Questionnaire> getEmployees() {
		Session session = getHibernateSession();
		String sql="FROM Questionnaire ORDER BY RAND()";
		Query query = session.createQuery(sql);
		query.setFirstResult(0);
		query.setMaxResults(6);
		List<Questionnaire> list=query.list();               
	      
		return list;
		
		
		
		/*{  
	        
	        	Questionnaire e=new Questionnaire();  
	            e.setQuestionId(rs.getInt(1));  
	            e.setQuestion(rs.getString(11));  
	            e.setOption1(rs.getString(7));  
	            e.setOption2(rs.getString(8));  
	            e.setOption3(rs.getString(9));  
	            e.setOption4(rs.getString(10));  
	            e.setAnswer(rs.getString(2));  
	            return e;  
	
	}*/
		}

	@SuppressWarnings("unchecked")
	@Override
	public List<Questionnaire> getEmployees(int pagenumber) {
		Session session = getHibernateSession();
		List<Questionnaire> list=null;
		final int pageSize = 3;
		String sql="FROM Questionnaire ORDER BY RAND()";
		Query query = session.createQuery(sql);
		query = query.setFirstResult(pageSize * (pagenumber - 1));
	      query.setMaxResults(pageSize);
	      list = query.list();
		return list;
	}
	}



