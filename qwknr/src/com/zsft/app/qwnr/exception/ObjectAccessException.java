package com.zsft.app.qwnr.exception;

public class ObjectAccessException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private Throwable _t;

	public ObjectAccessException(Throwable t) {
		_t = t;
	}

	public ObjectAccessException(String msg) {
		super(msg);
	}

	public ObjectAccessException() {

	}

	public final Throwable getThrowable() {
		return _t;
	}
}
