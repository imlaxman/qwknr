package com.zsft.app.qwnr.model.beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;



/**
 * The persistent class for the admin_user_login database table.
 * 
 */
@Entity
@Table(name="admin_user_login")
public class AdminUserLogin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ADMIN_USER_ID")
	@GeneratedValue(generator="gen")
	@GenericGenerator(name="gen", strategy="foreign", parameters=@Parameter(name="property", value="adminUserReg"))
	private int adminUserId;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_LOGIN_DATE")
	private Date lastLoginDate;

	@Column(name="LOGIN_EMAIL")
	private String loginEmail;

	@Column(name="PASSWORD")
	private String password;

	
	//bi-directional many-to-one association to AdminUserReg
		/*@ManyToOne
		@JoinColumn(name="ADMIN_USER_ID")*/
	@OneToOne
	@PrimaryKeyJoinColumn
	private AdminUserReg adminUserReg;
	
	public AdminUserLogin() {
	}

	public int getAdminUserId() {
		return adminUserId;
	}

	public void setAdminUserId(int adminUserId) {
		this.adminUserId = adminUserId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getLastLoginDate() {
		return this.lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public String getLoginEmail() {
		return this.loginEmail;
	}

	public void setLoginEmail(String loginEmail) {
		this.loginEmail = loginEmail;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public AdminUserReg getAdminUserReg() {
		return this.adminUserReg;
	}

	public void setAdminUserReg(AdminUserReg adminUserReg) {
		this.adminUserReg = adminUserReg;
	}

}