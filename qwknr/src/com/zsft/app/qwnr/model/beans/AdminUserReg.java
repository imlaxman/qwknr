package com.zsft.app.qwnr.model.beans;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the admin_user_reg database table.
 * 
 */
@Entity
@Table(name="admin_user_reg")
public class AdminUserReg implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name="ADMIN_USER_ID", unique = true, nullable = false)
	private int adminUserId;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="LAST_NAME")
	private String lastName;

	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="MOBILE")
	private String mobile;
	

	@OneToOne(mappedBy="adminUserReg", cascade=CascadeType.ALL)
	private AdminUserLogin adminUserLogin;

	//bi-directional many-to-one association to AdminUserLogin
	/*@OneToMany(mappedBy="adminUserReg")
	private List<AdminUserLogin> adminUserLogins;*/
	
	
	public AdminUserReg() {
	}

	public int getAdminUserId() {
		return this.adminUserId;
	}

	public void setAdminUserId(int adminUserId) {
		this.adminUserId = adminUserId;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/*public List<AdminUserLogin> getAdminUserLogins() {
		return this.adminUserLogins;
	}

	public void setAdminUserLogins(List<AdminUserLogin> adminUserLogins) {
		this.adminUserLogins = adminUserLogins;
	}*/

	/*public AdminUserLogin addAdminUserLogin(AdminUserLogin adminUserLogin) {
		getAdminUserLogins().add(adminUserLogin);
		adminUserLogin.setAdminUserReg(this);

		return adminUserLogin;
	}

	public AdminUserLogin removeAdminUserLogin(AdminUserLogin adminUserLogin) {
		getAdminUserLogins().remove(adminUserLogin);
		adminUserLogin.setAdminUserReg(null);

		return adminUserLogin;
	}
*/
	public AdminUserLogin getAdminUserLogin() {
		return adminUserLogin;
	}

	public void setAdminUserLogin(AdminUserLogin adminUserLogin) {
		this.adminUserLogin = adminUserLogin;
	}

}