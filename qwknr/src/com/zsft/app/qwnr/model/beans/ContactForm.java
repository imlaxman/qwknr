package com.zsft.app.qwnr.model.beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the contact_form database table.
 * 
 */
@Entity
@Table(name="contact_form")
public class ContactForm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CONTACT_FORM_ID")
	private String contactFormId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIME")
	private Date creationTime;

	@Column(name="EMAIL")
	private String email;

	@Column(name="MESSAGE")
	private String message;

	@Column(name="NAME")
	private String name;

	@Column(name="SUBJECT")
	private String subject;

	public ContactForm() {
	}

	public String getContactFormId() {
		return this.contactFormId;
	}

	public void setContactFormId(String contactFormId) {
		this.contactFormId = contactFormId;
	}

	public Date getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}