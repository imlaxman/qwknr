package com.zsft.app.qwnr.model.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the levels database table.
 * 
 */	
@Entity
@Table(name="levels")
public class Level implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="LEVEL_ID")
	private int levelId;

	@Column(name="LEVEL_DESCRIPTION")
	private String levelDescription;

	@Column(name="LEVEL_NAME")
	private String levelName;

	//bi-directional many-to-one association to Questionnaire
	
	/*@OneToMany(mappedBy="level")
	private List<Questionnaire> questionnaires;
*/
	public Level() {
	}

	public int getLevelId() {
		return this.levelId;
	}

	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	public String getLevelDescription() {
		return this.levelDescription;
	}

	public void setLevelDescription(String levelDescription) {
		this.levelDescription = levelDescription;
	}

	public String getLevelName() {
		return this.levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	/*public List<Questionnaire> getQuestionnaires() {
		return this.questionnaires;
	}

	public void setQuestionnaires(List<Questionnaire> questionnaires) {
		this.questionnaires = questionnaires;
	}*/

	/*public Questionnaire addQuestionnaire(Questionnaire questionnaire) {
		getQuestionnaires().add(questionnaire);
		questionnaire.setLevel(this);

		return questionnaire;
	}

	public Questionnaire removeQuestionnaire(Questionnaire questionnaire) {
		getQuestionnaires().remove(questionnaire);
		questionnaire.setLevel(null);

		return questionnaire;
	}*/

}