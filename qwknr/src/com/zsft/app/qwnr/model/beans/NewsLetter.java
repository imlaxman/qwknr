package com.zsft.app.qwnr.model.beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the news_letter database table.
 * 
 */
@Entity
@Table(name="news_letter")
public class NewsLetter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="NEWS_LETTER_ID")
	private String newsLetterId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIME")
	private Date creationTime;

	@Column(name="EMAIL")
	private String email;

	public NewsLetter() {
	}

	public String getNewsLetterId() {
		return this.newsLetterId;
	}

	public void setNewsLetterId(String newsLetterId) {
		this.newsLetterId = newsLetterId;
	}

	public Date getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}