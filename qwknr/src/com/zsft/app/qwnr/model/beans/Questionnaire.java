package com.zsft.app.qwnr.model.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the questionnaires database table.
 * 
 */
@Entity
@Table(name="questionnaires")
public class Questionnaire implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="QUESTION_ID")
	private int questionId;

	@Column(name="CREATED_BY")
	private int createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="OPTION_1")
	private String option1;

	@Column(name="OPTION_2")
	private String option2;

	@Column(name="OPTION_3")
	private String option3;

	@Column(name="OPTION_4")
	private String option4;
	
	@Column(name="ANSWER")
	private String answer;

	@Column(name="QUESTION")
	private String question;

	@Column(name="LEVEL_ID")
	private int levelId;
	
	@Column(name="CATEGORY_ID")
	private int categoryId;
	
	//bi-directional many-to-one association to Topic
	
	/*@ManyToOne
	@JoinColumn(name="TOPIC_ID")
	private Topic topic;
*/
	//bi-directional many-to-one association to Level
	
/*	@ManyToOne
	@JoinColumn(name="LEVEL_ID")
	private Level level;*/

	//bi-directional many-to-one association to UserQuestionnairesCompletion
	
	/*@OneToMany(mappedBy="questionnaire")
	private List<UserQuestionnairesCompletion> userQuestionnairesCompletions;
	*/

	public int getLevelId() {
		return levelId;
	}

	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	public Questionnaire() {
	}

	public int getQuestionId() {
		return this.questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public int getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getOption1() {
		return this.option1;
	}

	public void setOption1(String option1) {
		this.option1 = option1;
	}

	public String getOption2() {
		return this.option2;
	}

	public void setOption2(String option2) {
		this.option2 = option2;
	}

	public String getOption3() {
		return this.option3;
	}

	public void setOption3(String option3) {
		this.option3 = option3;
	}

	public String getOption4() {
		return this.option4;
	}

	public void setOption4(String option4) {
		this.option4 = option4;
	}

	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	/*public Topic getTopic() {
		return this.topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}
*/
	/*public Level getLevel() {
		return this.level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}*/

	

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	/*public List<UserQuestionnairesCompletion> getUserQuestionnairesCompletions() {
		return this.userQuestionnairesCompletions;
	}
	
	public void setUserQuestionnairesCompletions(List<UserQuestionnairesCompletion> userQuestionnairesCompletions) {
		this.userQuestionnairesCompletions = userQuestionnairesCompletions;
	}*/

	/*public UserQuestionnairesCompletion addUserQuestionnairesCompletion(UserQuestionnairesCompletion userQuestionnairesCompletion) {
		getUserQuestionnairesCompletions().add(userQuestionnairesCompletion);
		userQuestionnairesCompletion.setQuestionnaire(this);

		return userQuestionnairesCompletion;
	}

	public UserQuestionnairesCompletion removeUserQuestionnairesCompletion(UserQuestionnairesCompletion userQuestionnairesCompletion) {
		getUserQuestionnairesCompletions().remove(userQuestionnairesCompletion);
		userQuestionnairesCompletion.setQuestionnaire(null);

		return userQuestionnairesCompletion;
	}*/

}