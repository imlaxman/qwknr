package com.zsft.app.qwnr.model.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the topics database table.
 * 
 */
@Entity
@Table(name="topics")
public class Topic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="TOPIC_ID")
	private int topicId;

	@Column(name="TOPIC_DESCRIPTION")
	private String topicDescription;

	@Column(name="TOPIC_NAME")
	private String topicName;

	//bi-directional many-to-one association to Questionnaire
	/*@OneToMany(mappedBy="topic")
	private List<Questionnaire> questionnaires;
*/
	
	public Topic() {
	}

	public int getTopicId() {
		return this.topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public String getTopicDescription() {
		return this.topicDescription;
	}

	public void setTopicDescription(String topicDescription) {
		this.topicDescription = topicDescription;
	}

	public String getTopicName() {
		return this.topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	/*public List<Questionnaire> getQuestionnaires() {
		return this.questionnaires;
	}

	public void setQuestionnaires(List<Questionnaire> questionnaires) {
		this.questionnaires = questionnaires;
	}*/
/*
	public Questionnaire addQuestionnaire(Questionnaire questionnaire) {
		getQuestionnaires().add(questionnaire);
		questionnaire.setTopic(this);

		return questionnaire;
	}

	public Questionnaire removeQuestionnaire(Questionnaire questionnaire) {
		getQuestionnaires().remove(questionnaire);
		questionnaire.setTopic(null);

		return questionnaire;
	}
*/
}