package com.zsft.app.qwnr.model.beans;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * The persistent class for the user_levels_completed database table.
 * 
 */
@Entity
@Table(name="user_levels_completed")
public class UserLevelsCompleted implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name="USER_LEVEL_COMPLETION_ID", unique = true, nullable = false)
	private String userLevelCompletionId;
	

	@Column(name="LEVEL_ID")
	private int levelId;

	/*@Id
	@GeneratedValue(strategy = IDENTITY)*/
	
	
	public String getUserLevelCompletionId() {
		return userLevelCompletionId;
	}

	public void setUserLevelCompletionId(String userLevelCompletionId) {
		this.userLevelCompletionId = userLevelCompletionId;
	}

	@ManyToOne
	@JoinColumn(name="USER_ID")
	private UserReg userReg;
	
	//@Column(name="USER_ID")
	//private String userId;
	//bi-directional many-to-one association to UserReg
	
	

	public UserLevelsCompleted() {
	}

	public int getLevelId() {
		return this.levelId;
	}

	/*public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}*/

	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	public UserReg getUserReg() {
		return this.userReg;
	}

	public void setUserReg(UserReg userReg) {
		this.userReg = userReg;
	}

}