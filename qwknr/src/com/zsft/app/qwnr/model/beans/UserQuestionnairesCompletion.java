package com.zsft.app.qwnr.model.beans;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_questionnaires_completion")
public class UserQuestionnairesCompletion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name="USER_QUESTIONNAIRES_COMPLETION_ID", unique = true, nullable = false)
	private String userQuestionnairesCompletionId;
	
	
	@Column(name="COMPLETED_IN_SECONDS")
	private int completedInSeconds;

	@Column(name="QUESTION_SCORE")
	private int questionScore;
	
	@Column(name="QUESTION_ID")
	private int questionId;
	
	@Column(name="USER_ID")
	private String userId;


	//bi-directional many-to-one association to Questionnaire
	/*@ManyToOne
	@JoinColumn(name="QUESTION_ID")
	private Questionnaire questionnaire;*/

	public String getUserQuestionnairesCompletionId() {
		return userQuestionnairesCompletionId;
	}

	public void setUserQuestionnairesCompletionId(
			String userQuestionnairesCompletionId) {
		this.userQuestionnairesCompletionId = userQuestionnairesCompletionId;
	}

	//bi-directional many-to-one association to UserReg
	/*@ManyToOne
	@JoinColumn(name="USER_ID")
	private UserReg userReg;
*/
	public UserQuestionnairesCompletion() {
	}

	public int getCompletedInSeconds() {
		return this.completedInSeconds;
	}

	public void setCompletedInSeconds(int completedInSeconds) {
		this.completedInSeconds = completedInSeconds;
	}

	public int getQuestionScore() {
		return this.questionScore;
	}

	public void setQuestionScore(int questionScore) {
		this.questionScore = questionScore;
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/*public Questionnaire getQuestionnaire() {
		return this.questionnaire;
	}

	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}

	public UserReg getUserReg() {
		return this.userReg;
	}

	public void setUserReg(UserReg userReg) {
		this.userReg = userReg;
	}*/
	
	
}