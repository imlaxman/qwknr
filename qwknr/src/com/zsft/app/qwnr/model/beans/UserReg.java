package com.zsft.app.qwnr.model.beans;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the user_reg database table.
 * 
 */
@Entity
@Table(name="user_reg")
public class UserReg implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name="USER_ID", unique = true, nullable = false)
	private String userId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="LAST_NAME")
	private String lastName;

	@Column(name="MOBILE")
	private String mobile;

	//bi-directional many-to-one association to UserLevelsCompleted
	@OneToMany(mappedBy="userReg")
	private Set<UserLevelsCompleted> userLevelsCompleted;

	//bi-directional many-to-one association to UserLogin
	
	//@OneToMany(mappedBy="userReg")
	@OneToOne(mappedBy="userReg", cascade=CascadeType.ALL)
	private UserLogin userLogin;

	//bi-directional many-to-one association to UserQuestionnairesCompletion
	
	//@OneToMany(mappedBy="userReg")
	//private List<UserQuestionnairesCompletion> userQuestionnairesCompletions;

	public UserReg() {
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Set<UserLevelsCompleted> getUserLevelsCompleted() {
		return userLevelsCompleted;
	}

	public void setUserLevelsCompleted(Set<UserLevelsCompleted> userLevelsCompleted) {
		this.userLevelsCompleted = userLevelsCompleted;
	}

	public UserLogin getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(UserLogin userLogin) {
		this.userLogin = userLogin;
	}

	/*public List<UserLevelsCompleted> getUserLevelsCompleteds() {
		return this.userLevelsCompleteds;
	}

	public void setUserLevelsCompleteds(List<UserLevelsCompleted> userLevelsCompleteds) {
		this.userLevelsCompleteds = userLevelsCompleteds;
	}
*/
	/*public UserLevelsCompleted addUserLevelsCompleted(UserLevelsCompleted userLevelsCompleted) {
		getUserLevelsCompleteds().add(userLevelsCompleted);
		userLevelsCompleted.setUserReg(this);

		return userLevelsCompleted;
	}

	public UserLevelsCompleted removeUserLevelsCompleted(UserLevelsCompleted userLevelsCompleted) {
		getUserLevelsCompleteds().remove(userLevelsCompleted);
		userLevelsCompleted.setUserReg(null);

		return userLevelsCompleted;
	}*/

	/*public List<UserLogin> getUserLogins() {
		return this.userLogins;
	}

	public void setUserLogins(List<UserLogin> userLogins) {
		this.userLogins = userLogins;
	}*/

	/*public UserLogin addUserLogin(UserLogin userLogin) {
		getUserLogins().add(userLogin);
		userLogin.setUserReg(this);

		return userLogin;
	}

	public UserLogin removeUserLogin(UserLogin userLogin) {
		getUserLogins().remove(userLogin);
		userLogin.setUserReg(null);

		return userLogin;
	}*/

	/*public List<UserQuestionnairesCompletion> getUserQuestionnairesCompletions() {
		return this.userQuestionnairesCompletions;
	}

	public void setUserQuestionnairesCompletions(List<UserQuestionnairesCompletion> userQuestionnairesCompletions) {
		this.userQuestionnairesCompletions = userQuestionnairesCompletions;
	}
*/
	/*public UserQuestionnairesCompletion addUserQuestionnairesCompletion(UserQuestionnairesCompletion userQuestionnairesCompletion) {
		getUserQuestionnairesCompletions().add(userQuestionnairesCompletion);
		userQuestionnairesCompletion.setUserReg(this);

		return userQuestionnairesCompletion;
	}

	public UserQuestionnairesCompletion removeUserQuestionnairesCompletion(UserQuestionnairesCompletion userQuestionnairesCompletion) {
		getUserQuestionnairesCompletions().remove(userQuestionnairesCompletion);
		userQuestionnairesCompletion.setUserReg(null);

		return userQuestionnairesCompletion;
	}
*/
}