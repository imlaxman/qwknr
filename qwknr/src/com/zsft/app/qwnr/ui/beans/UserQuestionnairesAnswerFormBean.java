package com.zsft.app.qwnr.ui.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.zsft.app.qwnr.model.beans.Questionnaire;
import com.zsft.app.qwnr.model.beans.UserReg;


/**
 * The persistent class for the user_questionnaires_completion database table.
 * 
 */
public class UserQuestionnairesAnswerFormBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String userId;
	private int completedInSeconds;
	private int questionScore;
	private Questionnaire questionnaire;
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getCompletedInSeconds() {
		return completedInSeconds;
	}
	public void setCompletedInSeconds(int completedInSeconds) {
		this.completedInSeconds = completedInSeconds;
	}
	public int getQuestionScore() {
		return questionScore;
	}
	public void setQuestionScore(int questionScore) {
		this.questionScore = questionScore;
	}
	public Questionnaire getQuestionnaire() {
		return questionnaire;
	}
	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}