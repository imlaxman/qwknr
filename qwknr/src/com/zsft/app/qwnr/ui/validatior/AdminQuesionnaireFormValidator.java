package com.zsft.app.qwnr.ui.validatior;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.zsft.app.qwnr.bo.AdminBoImpl;
import com.zsft.app.qwnr.constants.FormConstants;
import com.zsft.app.qwnr.dao.AdminDaoImpl;
import com.zsft.app.qwnr.ui.beans.FormBean;
import com.zsft.app.qwnr.util.StringUtils;

public class AdminQuesionnaireFormValidator implements Validator{

	@Autowired
	private AdminBoImpl adminBo;

	@Override
	public boolean supports(Class<?> paramClass) {
		return FormBean.class.equals(paramClass);
	}
	
	public void validate(Object obj, Errors errors) {
		FormBean form = (FormBean) obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "valid.firstname");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "valid.lastname");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mobile", "valid.mobile");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "loginEmail", "valid.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "loginPassword", "valid.password");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "valid.confirmpassword");
		
		
				
	}
}
