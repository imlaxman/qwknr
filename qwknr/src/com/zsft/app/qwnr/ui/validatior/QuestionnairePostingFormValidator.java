package com.zsft.app.qwnr.ui.validatior;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.zsft.app.qwnr.bo.UserBoImpl;
import com.zsft.app.qwnr.ui.beans.FormBean;
import com.zsft.app.qwnr.ui.beans.QuestionnaireFormBean;

public class QuestionnairePostingFormValidator implements Validator{

	@Autowired
	private UserBoImpl userBo;

	@Override
	public boolean supports(Class<?> paramClass) {
		return QuestionnaireFormBean.class.equals(paramClass);
	}

	
	public void validate(Object obj, Errors errors) {
		QuestionnaireFormBean form = (QuestionnaireFormBean) obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "question", "valid.question");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "answer", "valid.answer");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "option1", "valid.option1");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "option2", "valid.option2");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "option3", "valid.option3");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "option4", "valid.option4");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "questionLevelName", "valid.questionlevelname");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "questionCategory", "valid.questioncategory");
		
		
		
		
	}

}
