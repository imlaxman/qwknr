package com.zsft.app.qwnr.ui.validatior;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.zsft.app.qwnr.bo.UserBoImpl;
import com.zsft.app.qwnr.constants.FormConstants;
import com.zsft.app.qwnr.ui.beans.FormBean;
import com.zsft.app.qwnr.util.StringUtils;

public class UserRegistrationFormValidator implements Validator{

	@Autowired
	private UserBoImpl userBo;

	@Override
	public boolean supports(Class<?> paramClass) {
		return FormBean.class.equals(paramClass);
	}
	
	public void validate(Object obj, Errors errors) {
		FormBean form = (FormBean) obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "valid.firstname");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "valid.lastname");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mobile", "valid.mobile");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "loginEmail", "valid.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "loginPassword", "valid.password");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "valid.confirmpassword");

		if(!StringUtils.isFieldBetween3nd32(form.getFirstName())) {
			errors.rejectValue("firstName","valid.length3to32");
		}
		else if(StringUtils.isFieldNotEmpty(form.getFirstName()) && form.getFirstName().matches(FormConstants.NUMBER_PATTERN)){
			errors.rejectValue("firstName","valid.chars");
		}
		
		if(!StringUtils.isFieldBetween3nd32(form.getLastName())) {
			errors.rejectValue("lastName","valid.length3to32");
		}
		else if(StringUtils.isFieldNotEmpty(form.getLastName()) && form.getLastName().matches(FormConstants.NUMBER_PATTERN)){
			errors.rejectValue("lastName","valid.chars");
		}
		
		if(!StringUtils.isFieldEmpty(form.getLoginEmail()) && !form.getLoginEmail().matches(FormConstants.EMAIL_PATTERN)) {
			errors.rejectValue("loginEmail","valid.email");
		}
		
		if(!StringUtils.isFieldEmpty(form.getMobile()) && !form.getMobile().matches(FormConstants.MOBILE_PATTERN)) {
			errors.rejectValue("loginEmail","valid.email");
		}
		
		if(!StringUtils.isFieldEmpty(form.getLoginPassword()) && !StringUtils.isFieldBetween8nd16(form.getLoginPassword())) {
			errors.rejectValue("loginPassword","valid.passwordlength");
		}
		
		if (!form.getLoginPassword().equals(form.getConfirmPassword())) {
			errors.rejectValue("confirmPassword", "valid.passwordsdifferent");
		}	
	}
}
