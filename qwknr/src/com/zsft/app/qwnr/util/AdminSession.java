package com.zsft.app.qwnr.util;

import java.util.Date;

public class AdminSession {

	private int adminId;;
	private String adminEmail;
	private String firstName;
	private String lastName;
	private String contactNumber;
	private String adminType;
	private Date lastUpdateDate;
	
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public String getAdminType() {
		return adminType;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public void setAdminType(String adminType) {
		this.adminType = adminType;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	/*public Long getAdminId() {
		return adminId;
	}
	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}*/
	public String getAdminEmail() {
		return adminEmail;
	}
	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}
	public int getAdminId() {
		return adminId;
	}
	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}
	
	

}
