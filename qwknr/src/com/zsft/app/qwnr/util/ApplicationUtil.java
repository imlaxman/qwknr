package com.zsft.app.qwnr.util;

import java.io.File;
import java.io.IOException;

import org.springframework.context.ApplicationContext;

public class ApplicationUtil {

	private static ApplicationContext context;
	
	private ApplicationUtil(){
		
	}
	
	public static void setContext(ApplicationContext context) {
		ApplicationUtil.context = context;
	}
	
	public static ApplicationContext getContext() {
		return context;
	}
	
	public static Object getBean(String id) {
		return context.getBean(id);
	}
	
	public static File getWebinfPath() throws IOException {
		//return context.getResource("src").getFile();
		return new File("src");
	}
	
	
	
	
}
