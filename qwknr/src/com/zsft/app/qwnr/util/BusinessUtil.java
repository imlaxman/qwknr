package com.zsft.app.qwnr.util;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.zsft.app.qwnr.bo.AdminBoImpl;
import com.zsft.app.qwnr.bo.UserBoImpl;
import com.zsft.app.qwnr.constants.CommonConstants;

public class BusinessUtil implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(BusinessUtil.class);
	
	@Autowired
	private AdminBoImpl adminBo;
	
	
 	@Autowired
    private UserBoImpl userBo;	
	
	
	
	public static String md5CodeGen(String input) {
	    String md5 = null;
	    if(null == input) return null;
	    input = input.trim();
	    try {
	    //Create MessageDigest object for MD5
	    MessageDigest digest = MessageDigest.getInstance("MD5");
	    //Update input string in message digest
	    digest.update(input.getBytes(), 0, input.length());
	    //Converts message digest value in base 16 (hex) 
	    md5 = new BigInteger(1, digest.digest()).toString(16);
	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    }
	    return md5;
	}
	
	
	public static String get_SHA_256_SecurePassword(String passwordToHash, String salt) throws NoSuchAlgorithmException     {
	        //Use MessageDigest md = MessageDigest.getInstance("SHA-256");
		 	  salt = getSalt();
	    	 String generatedPassword = null;
	         try {
	             MessageDigest md = MessageDigest.getInstance("SHA-256");
	             md.update(salt.getBytes());
	             byte[] bytes = md.digest(passwordToHash.getBytes());
	             StringBuilder sb = new StringBuilder();
	             for(int i=0; i< bytes.length ;i++) {
	                 sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
	             }
	             generatedPassword = sb.toString();
	         } 
	         catch (NoSuchAlgorithmException e)   {
	             e.printStackTrace();
	         }
	         return generatedPassword;
	    }
	
	public static String getSalt() throws NoSuchAlgorithmException  {
	        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
	        byte[] salt = new byte[16];
	        sr.nextBytes(salt);
	        return salt.toString();
	    }
	 
	
	
	
	
	
	
	public void manageUserSessionData(HttpSession session){
					UserSession  jskrSession =  (UserSession) session.getAttribute(CommonConstants.JSKR_SESSION);
					if(jskrSession !=null){
						//jskrBo.updateLastUpdatedDate(jskrSession.getJskId(), jskrSession.getJskrReferenceCd());
						/*List<JskJobApplications> jobApplicationsList =	jskrSession.getJskJobApplicationsList();
						if(jobApplicationsList !=null && jobApplicationsList.size()>0){
							log.info("_________CustomSessionListner jobApplicationsList: "+jobApplicationsList.size());
							jskrBo.saveJskrJobApplications(jobApplicationsList);
						}*/
						
						/*List<JskSearchHistory> jskSearchHistoryList = jskrSession.getJskSearchHistoryList();
						if(jskSearchHistoryList !=null && jskSearchHistoryList.size()>0){
							log.info("_________CustomSessionListner jskSearchHistoryList: "+jskSearchHistoryList.size());
							jskrBo.saveJskrSearchHistry(jskSearchHistoryList);
						}*/
					}
		}
		
	
		public void manageAdminSessionData(HttpSession session){
					AdminSession adminSession = (AdminSession) session.getAttribute(CommonConstants.EMPLR_SESSION);
					if(adminSession !=null){
						//emplrBo.updateEmplrLastLoginDate(emplrSession.getEmplrId(), emplrSession.getEmplrEmail());
						/*List<EmplrPrflsViewesDownloads> emplrPrflsViewedDownloadedList = emplrSession.getEmplrPrflsViewesDownloads();
						if(emplrPrflsViewedDownloadedList !=null && emplrPrflsViewedDownloadedList.size()>0){
							log.info("_________CustomSessionListner emplrPrflsViewedDownloadedList: "+emplrPrflsViewedDownloadedList.size());
							emplrBo.saveEmplrPrflsViewedDownloaded(emplrPrflsViewedDownloadedList);
							
						}
						List<EmplrPrflsSearchHstry> emplrPrflsSearchHstryList = emplrSession.getEmplrPrflsSearchHstryList();
						if(emplrPrflsSearchHstryList !=null && emplrPrflsSearchHstryList.size()>0){
							log.info("_________CustomSessionListner emplrPrflsSearchHstryList: "+emplrPrflsSearchHstryList.size());
							emplrBo.saveEmplrPrflsSearchHstry(emplrPrflsSearchHstryList);
						}*/
					}
				
				}
	
	
		
	
	
		
		public void manageSessionData(HttpSession session){
			
			//	String userType = (String) FacesUtils.getUserTypeFromSession();
				String userType = (String) session.getAttribute(CommonConstants.USER_TYPE);
				log.info("%%%%%%%%%%%%%_________CustomSessionListner called managing session data for  userType...: "+userType);
				if(userType != null && !userType.equals("")){
					if(userType.equals(CommonConstants.USER_TYPE_JSKR)){
						UserSession  jskrSession =  (UserSession) session.getAttribute(CommonConstants.JSKR_SESSION);
						if(jskrSession !=null){
							//jskrBo.updateLastUpdatedDate(jskrSession.getJskId(), jskrSession.getJskrReferenceCd());
							/*List<JskJobApplications> jobApplicationsList =	jskrSession.getJskJobApplicationsList();
							if(jobApplicationsList !=null && jobApplicationsList.size()>0){
								log.info("_________CustomSessionListner jobApplicationsList: "+jobApplicationsList.size());
								jskrBo.saveJskrJobApplications(jobApplicationsList);
							}*/
							
							/*List<JskSearchHistory> jskSearchHistoryList = jskrSession.getJskSearchHistoryList();
							if(jskSearchHistoryList !=null && jskSearchHistoryList.size()>0){
								log.info("_________CustomSessionListner jskSearchHistoryList: "+jskSearchHistoryList.size());
								jskrBo.saveJskrSearchHistry(jskSearchHistoryList);
							}*/
						}
						
					}else if(userType.equals(CommonConstants.USER_TYPE_EMPLOYER)){
						AdminSession emplrSession = (AdminSession) session.getAttribute(CommonConstants.EMPLR_SESSION);
						if(emplrSession !=null){
							//emplrBo.updateEmplrLastLoginDate(emplrSession.getEmplrId(), emplrSession.getEmplrEmail());
							/*List<EmplrPrflsViewesDownloads> emplrPrflsViewedDownloadedList = emplrSession.getEmplrPrflsViewesDownloads();
							if(emplrPrflsViewedDownloadedList !=null && emplrPrflsViewedDownloadedList.size()>0){
								log.info("_________CustomSessionListner emplrPrflsViewedDownloadedList: "+emplrPrflsViewedDownloadedList.size());
								emplrBo.saveEmplrPrflsViewedDownloaded(emplrPrflsViewedDownloadedList);
								
							}
							List<EmplrPrflsSearchHstry> emplrPrflsSearchHstryList = emplrSession.getEmplrPrflsSearchHstryList();
							if(emplrPrflsSearchHstryList !=null && emplrPrflsSearchHstryList.size()>0){
								log.info("_________CustomSessionListner emplrPrflsSearchHstryList: "+emplrPrflsSearchHstryList.size());
								emplrBo.saveEmplrPrflsSearchHstry(emplrPrflsSearchHstryList);
							}*/
						}
					}else if(userType.equals(CommonConstants.USER_TYPE_ADMIN)){
					}
				}
			}
				
		
	
	String month ="MULTIBRANCHE";
	public String JANUARY ="M";
	public String FEBRAUARY ="U";
	public String MARCH ="L";
	public String APRIL ="T";
	public String MAY ="I";
	public String JUNE ="B";
	public String JULY ="R";
	public String AUGUST ="A";
	public String SEPTEMBER ="N";
	public String OCTOBER ="C";
	public String NOVEMBER ="H";
	public String DECEMBER ="E";
	
	public static String formatMonth(int month){
		String mnth="";
		if(month == 0){
			mnth = "M";
		}else if(month == 1){
			mnth = "U";
		}else if(month == 2){
			mnth = "L";
		}else if(month == 3){
			mnth = "T";
		}else if(month == 4){
			mnth = "I";
		}else if(month == 5){
			mnth = "B";
		}else if(month == 6){
			mnth = "R";
		}else if(month == 7){
			mnth = "A";
		}else if(month == 8){
			mnth = "N";
		}else if(month == 9){
			mnth = "C";
		}else if(month == 10){
			mnth = "H";
		}else if(month == 11){
			mnth = "E";
		}
		return mnth;
	}
	public static String formatYr(int year){
		String yr="";
		if(year == 2013){
			yr = "B";
		}if(year == 2014){
			yr = "C";
		}if(year == 2015){
			yr = "D";
		}if(year == 2016){
			yr = "E";
		}if(year == 2017){
			yr = "F";
		}if(year == 2018){
			yr = "G";
		}if(year == 2019){
			yr = "H";
		}if(year == 2020){
			yr = "I";
		}if(year == 2021){
			yr = "J";
		}if(year == 2022){
			yr = "K";
		}if(year == 2023){
			yr = "L";
		}if(year == 2024){
			yr = "M";
		}if(year == 2025){
			yr = "N";
		}if(year == 2026){
			yr = "O";
		}if(year == 2027){
			yr = "P";
		}if(year == 2028){
			yr = "Q";
		}if(year == 2029){
			yr = "R";
		}if(year == 2030){
			yr = "S";
		}
		return yr;
	}
	
	
	public static String formatLoc2Letter(String st){
		if(st.equalsIgnoreCase("a")){
			st = "B";
		}else if(st.equalsIgnoreCase("b")){
			st = "C";
		}else if(st.equalsIgnoreCase("c")){
			st = "D";
		}else if(st.equalsIgnoreCase("d")){
			st = "E";
		}else if(st.equalsIgnoreCase("e")){
			st = "F";
		}else if(st.equalsIgnoreCase("f")){
			st = "G";
		}else if(st.equalsIgnoreCase("g")){
			st = "H";
		}else if(st.equalsIgnoreCase("h")){
			st = "I";
		}else if(st.equalsIgnoreCase("i")){
			st = "J";
		}else if(st.equalsIgnoreCase("j")){
			st = "K";
		}else if(st.equalsIgnoreCase("k")){
			st = "L";
		}else if(st.equalsIgnoreCase("l")){
			st = "M";
		}else if(st.equalsIgnoreCase("m")){
			st = "N";
		}else if(st.equalsIgnoreCase("n")){
			st = "O";
		}else if(st.equalsIgnoreCase("o")){
			st = "P";
		}else if(st.equalsIgnoreCase("p")){
			st = "Q";
		}else if(st.equalsIgnoreCase("q")){
			st = "R";
		}else if(st.equalsIgnoreCase("r")){
			st = "S";
		}else if(st.equalsIgnoreCase("s")){
			st = "T";
		}else if(st.equalsIgnoreCase("t")){
			st = "U";
		}else if(st.equalsIgnoreCase("u")){
			st = "V";
		}else if(st.equalsIgnoreCase("v")){
			st = "W";
		}else if(st.equalsIgnoreCase("w")){
			st = "X";
		}else if(st.equalsIgnoreCase("x")){
			st = "Y";
		}else if(st.equalsIgnoreCase("y")){
			st = "Z";
		}else if(st.equalsIgnoreCase("y")){
			st = "A";
		}
		return st;
	}
	
	
	
	public static String formatJobRole(String role){
		String jobRole="";
		if(role.equalsIgnoreCase("Accounting/Tax/Company Secretary/Audit")){
			jobRole = "1";
		}else if(role.equalsIgnoreCase("Accounting/Tax")){
			jobRole = "2";
		}
		
		
		return jobRole;
	}
	
	public static String buildReferenceCode(String sequenceId,String location){
		String refCd="";
		Date d = new Date();
		System.out.println(d);
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(d);
	    int year = cal.get(Calendar.YEAR);
	    int month = cal.get(Calendar.MONTH);
	    String firstChr ="";
	    String lastChr="";
	    if((location != null)&&(location.length()>1)){
	    	firstChr = location.substring(0,1).toUpperCase();
			 lastChr= location.substring(location.length()-1,location.length());
			lastChr = formatLoc2Letter(lastChr);	
	    }
		//BB1000MI ..month+year
		//refCd = formatMonth(month)+formatYr(year)+sequenceId+firstChr+lastChr;
		refCd = firstChr+sequenceId+lastChr;		//M1000H ==>Mumbai,1000
		System.out.println("____"+refCd);

		return refCd;
	}
	
	public static String buildJobRefCd(String jobPostSeq,String role){
		String jobPostRefCd="";
		
		Date d = new Date();
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(d);
	    int year = cal.get(Calendar.YEAR);
	    int month = cal.get(Calendar.MONTH);
	    
		String firstChr = role.substring(0,1);
		//String lastChr= location.substring(location.length()-1,location.length());
		//lastChr = formatLoc2Letter(lastChr);
		//BB1000MI ..month+year
		
		jobPostRefCd = formatMonth(month)+formatYr(year)+jobPostSeq+firstChr;
		System.out.println("____"+jobPostRefCd);
		return jobPostRefCd;
	}
	
	
	private static final String charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static String getRandomPassword(int length) {
	    Random rand = new Random(System.currentTimeMillis());
	    StringBuilder sb = new StringBuilder();
	    for (int i = 0; i < length; i++) {
	        int pos = rand.nextInt(charset.length());
	        sb.append(charset.charAt(pos));
	    }
	    System.out.println("Generated Password: "+sb.toString());
	    return sb.toString();
	}
	
	
	public String prepareJobSearchQueryString(String skillsString,String locationsString,int minExp,int maxExp){
		String queryString="";
		String s = "from EmplrJobPostings where";
		String locStr = prepareSearchAlgorForLoc(locationsString);
		String skillStr = prepareSearchAlgorForSkill(skillsString);
		String expStr = prepareSearchAlgorForExperience(minExp,maxExp);
		queryString = s+locStr+skillStr+expStr;
		System.out.println(queryString);
		return queryString;
	}
	public String prepareSearchAlgorForLoc(String str){
		StringBuffer buf = new StringBuffer("");
		String s="jobLoc";
		str = str.replaceAll(",", " ");
		String[] splited = str.split("\\s+");
		for (int i = 0; i < splited.length; i++) {
			 buf.append(s);
			 buf.append("='");
			 buf.append(splited[i].trim());
			 buf.append("' or ");
		}
		String s1 = buf.toString();
		String s2 = buf.toString();
		String s3 = buf.toString();
		String s4 = buf.toString();
		
		if(splited.length-1!=0){
			String loc1 = "loc1";
			String loc1Result = s1.replaceAll("loc", loc1);
			buf.append(loc1Result);
		}if(splited.length-2>=0){
			String loc2 = "loc2";
			String loc2Result = s2.replaceAll("loc", loc2);
			buf.append(loc2Result);
		}if(splited.length-3>=0){
			String loc3 = "loc3";
			String loc3Result = s3.replaceAll("loc", loc3);
			buf.append(loc3Result);
		}if(splited.length-4>=0){
			String loc4 = "loc4";
			String loc4Result = s4.replaceAll("loc", loc4);
			buf.append(loc4Result);
		}
		String ss = StringUtils.substring(buf.toString(), 0, buf.toString().length()-3);
		buf = new StringBuffer(ss);
		buf.append("and ");
		//System.out.println(buf.toString());
		return buf.toString();
	}
	
	public String prepareSearchAlgorForSkill(String skillsString){
		StringBuffer buf = new StringBuffer();
		String s="keySkills";
		skillsString = skillsString.replaceAll(",", " ");
		String[] splited = skillsString.split("\\s+");
		for (int i = 0; i < splited.length; i++) {
			buf.append(s);
			buf.append("='");
			buf.append(splited[i].trim());
			buf.append("' or ");
		}
		String ss = StringUtils.substring(buf.toString(), 0, buf.toString().length()-3);
		buf = new StringBuffer(ss);
		buf.append("and ");
		return buf.toString();
	}
	
	public String prepareSearchAlgorForExperience(int minExp,int maxExp){
		String s="minExperience between "+minExp+" and "+maxExp;
		String s1=" or maxExperience between "+minExp+" and "+maxExp;
		return s+s1;
		
	}
	
	
	
	public static String getFileContentType(String fileExtention){
		String fileContentType =null;
		 	if(fileExtention.equals("txt"));
				fileContentType = "text/plain";
			if(fileExtention.equals("doc"));
				fileContentType = "application/msword";
			if(fileExtention.equals("pdf"));
				fileContentType = "application/pdf";
			if(fileExtention.equals("docx"));
				fileContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
		return fileContentType;
	}
	
	public static String getMonthFileFolderForJskr(int month){
		String mnth="";
		if(month == 0){
			mnth = "01";
		}else if(month == 1){
			mnth = "02";
		}else if(month == 2){
			mnth = "03";
		}else if(month == 3){
			mnth = "04";
		}else if(month == 4){
			mnth = "05";
		}else if(month == 5){
			mnth = "06";
		}else if(month == 6){
			mnth = "07";
		}else if(month == 7){
			mnth = "08";
		}else if(month == 8){
			mnth = "09";
		}else if(month == 9){
			mnth = "10";
		}else if(month == 10){
			mnth = "11";
		}else if(month == 11){
			mnth = "12";
		}
		return mnth;
	}
	
	
	
    
	
	public static void main(String[] args){
		String jskrSeq="1000";
		String emplrSeq="1000";
		String location ="Mumbai";
		String jobPostSeq="1000";
		String role="abc";
		
	/*	buildReferenceCode(jskrSeq, location);
		buildEmplrRefId(emplrSeq, location);
		buildJobRefCd(jobPostSeq, role);*/
		int length =8;
		getRandomPassword(length);
		
	}
}
