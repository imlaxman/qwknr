package com.zsft.app.qwnr.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

public class MailUtil {

	public static Session buildMailSession(String fromEmail,String password) {
		 
		String host = "mail.jumbojobs.com";
		String port = "25";
		String auth = "true";

		Properties mailProps = new Properties();
		mailProps.put("mail.transport.protocol", "smtp");
		mailProps.put("mail.host", host);
		mailProps.put("mail.from", fromEmail);
		mailProps.put("mail.smtp.port", port);
		mailProps.put("mail.smtp.auth", auth);
		/*mailProps.put("mail.smtp.ssl.enable", "true");*/
		/*mailProps.put("mail.smtp.starttls.enable", "true");
		mailProps.put("mail.smtp.socketFactory.port", 25);
		mailProps.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		mailProps.put("mail.smtp.socketFactory.fallback", "false");*/

	final PasswordAuthentication usernamePassword =  new PasswordAuthentication(fromEmail, password);
	Authenticator authenticatior = new Authenticator() {
		    protected PasswordAuthentication getPasswordAuthentication() {
		      return usernamePassword;
		    }
		  };
		  Session session = Session.getInstance(mailProps, authenticatior);
		  session.setDebug(true);
		  return session;
		}
	
	
}
