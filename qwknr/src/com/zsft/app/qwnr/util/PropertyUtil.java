package com.zsft.app.qwnr.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PropertyUtil {

	public static String getEmplrFilePathProperty(){
		Properties prop = new Properties();
		InputStream propertyfileInputStream = null;
		String emplrfilepath=null;
		try {
			//com.fsft.jumbojobs.resources
			propertyfileInputStream = new FileInputStream("com/fsft/jumbojobs/resources/filePath.properties");
			prop.load(propertyfileInputStream);
			emplrfilepath = prop.getProperty("emplrfilepath");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (propertyfileInputStream != null) {
				try {
					propertyfileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return emplrfilepath;
	}
	
	public static String getJskrFilePathProperty(){
		Properties prop = new Properties();
		InputStream propertyfileInputStream = null;
		String jskrfilepath=null;
		try {
			propertyfileInputStream = new FileInputStream("com/fsft/jumbojobs/resources/filePath.properties");
			prop.load(propertyfileInputStream);
			jskrfilepath = prop.getProperty("jskrfilepath");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (propertyfileInputStream != null) {
				try {
					propertyfileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return jskrfilepath;
	}
	
	
	public String getEnvironmentDetails(){
		Properties prop = new Properties();
		InputStream propertyfileInputStream = null;
		String environment=null;
		try {
			
			propertyfileInputStream  = this.getClass().getClassLoader().getResourceAsStream("app.properties");
			//propertyfileInputStream = new FileInputStream("src/app.properties");
			//propertyfileInputStream = new FileInputStream("src/app.properties");
			//properties.load(new FileInputStream("src/main/resources/application.properties"));
			
			prop.load(propertyfileInputStream);
			environment = prop.getProperty("env");
			System.out.println("**********environment "+environment);
			//if(environment==null)
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (propertyfileInputStream != null) {
				try {
					propertyfileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return environment;
	}
	
	
	public  String getfromEmail(){
		Properties prop = new Properties();
		InputStream propertyfileInputStream = null;
		String email=null;
		try {
			//propertyfileInputStream = new FileInputStream("src/app.properties");
			propertyfileInputStream  = this.getClass().getClassLoader().getResourceAsStream("app.properties");
			prop.load(propertyfileInputStream);
			email = prop.getProperty("fromEmail");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (propertyfileInputStream != null) {
				try {
					propertyfileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return email;
	}
	
	
	
	
	public  String getToEmailForTesting(){
		Properties prop = new Properties();
		InputStream propertyfileInputStream = null;
		String email=null;
		try {
			//propertyfileInputStream = new FileInputStream("src/app.properties");
			propertyfileInputStream  = this.getClass().getClassLoader().getResourceAsStream("app.properties");
			prop.load(propertyfileInputStream);
			email = prop.getProperty("toEmail");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (propertyfileInputStream != null) {
				try {
					propertyfileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return email;
	}
	
	
	public  String getToEmailForMailing(){
		Properties prop = new Properties();
		InputStream propertyfileInputStream = null;
		String email=null;
		try {
			//propertyfileInputStream = new FileInputStream("src/app.properties");
			propertyfileInputStream  = this.getClass().getClassLoader().getResourceAsStream("app.properties");
			prop.load(propertyfileInputStream);
			email = prop.getProperty("mailingToEmail");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (propertyfileInputStream != null) {
				try {
					propertyfileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return email;
	}
	
	
	public  String getToEmailForColBlog(){
		Properties prop = new Properties();
		InputStream propertyfileInputStream = null;
		String email=null;
		try {
			//propertyfileInputStream = new FileInputStream("src/app.properties");
			propertyfileInputStream  = this.getClass().getClassLoader().getResourceAsStream("app.properties");
			prop.load(propertyfileInputStream);
			email = prop.getProperty("mailingToColblog");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (propertyfileInputStream != null) {
				try {
					propertyfileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return email;
	}
	
	
	
	
	
	public List<String> getEmailPassowordsForEmailing(){
		Properties prop = new Properties();
		InputStream propertyfileInputStream = null;
		String fromEmail="";
		String password="";
		List<String> fromEmailDtls = new ArrayList<String>();
		try {
			propertyfileInputStream  = this.getClass().getClassLoader().getResourceAsStream("app.properties");
			//propertyfileInputStream = new FileInputStream("src/app.properties");
			//propertyfileInputStream = new FileInputStream("/app.properties");
			prop.load(propertyfileInputStream);
			fromEmail = prop.getProperty("fromEmail");
			password = prop.getProperty("password");
			 fromEmailDtls.add(fromEmail);
			 fromEmailDtls.add(password);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (propertyfileInputStream != null) {
				try {
					propertyfileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return fromEmailDtls;
	}
	
	
	public Object[] getFromEmailsAndPassowordsForEmailing(){
		Properties prop = new Properties();
		InputStream propertyfileInputStream = null;
		String fromEmail="";
		String password="";
		List<String> fromEmailDtls = new ArrayList<String>();
		try {
			propertyfileInputStream  = this.getClass().getClassLoader().getResourceAsStream("app.properties");
			//propertyfileInputStream = new FileInputStream("src/app.properties");
			//propertyfileInputStream = new FileInputStream("/app.properties");
			prop.load(propertyfileInputStream);
			fromEmail = prop.getProperty("fromEmail");
			fromEmail = prop.getProperty("fromEmail2");
			fromEmail = prop.getProperty("fromEmail3");
			fromEmail = prop.getProperty("fromEmail4");
			fromEmail = prop.getProperty("fromEmail5");
			password = prop.getProperty("password");
			 fromEmailDtls.add(fromEmail);
			 fromEmailDtls.add(password);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (propertyfileInputStream != null) {
				try {
					propertyfileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return fromEmailDtls.toArray();
	}
	
	public String getEnvironmentEmail(){
		Properties prop = new Properties();
		InputStream propertyfileInputStream = null;
		String email=null;
		try {
			//propertyfileInputStream = new FileInputStream("src/app.properties");
			propertyfileInputStream  = this.getClass().getClassLoader().getResourceAsStream("app.properties");
			prop.load(propertyfileInputStream);
			email = prop.getProperty("email");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (propertyfileInputStream != null) {
				try {
					propertyfileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return email;
	}
	
	
	public static void main(String[] args){
		//System.out.println(getEnvironmentDetails());
	}
}
