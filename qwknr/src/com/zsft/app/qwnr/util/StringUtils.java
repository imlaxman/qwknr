package com.zsft.app.qwnr.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.map.HashedMap;

public class StringUtils extends org.apache.commons.lang3.StringUtils{
	
	private static final SimpleDateFormat format = new SimpleDateFormat("MMddyyyy");
	
	/*private static final SimpleDateFormat timeStampFormat 
				= new SimpleDateFormat(ApplicationConfig.getProperty("db2.timestampformat"));
	private static final SimpleDateFormat fileSufixFormat 
				= new SimpleDateFormat(ApplicationConfig.getProperty("file.suffix.format"));
	private static final String fileExt = ApplicationConfig.getProperty("file.ext").trim();
*/	
	
	
	public static String getRegion(Date date) {
		return format.format(date);
	}
	
	/*public static String getTimestampStr(Date date) {
		return timeStampFormat.format(date);
	}
	
	public static String appendSuffix(String fileName) {
		String suffix = fileSufixFormat.format(Calendar.getInstance().getTime());		
		fileName = fileName.replace(fileExt, "");
		
		return fileName + "." + suffix + fileExt;
	}*/
	
	public static String toStringAsInParamList(List<String> list) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			if (i > 0) {
				buf.append(",");
			}
			buf.append("'");
			buf.append(list.get(i));
			buf.append("'");
		}
		return buf.toString();
	}
	
	
	public static String getFirstChar(String str){
		return str.substring(0, 1);
	}
	
	public static String getLastChar(String str){
		return str.substring(str.length()-1, str.length());
	}
	
		
	public static String prepareJobSearchQueryString(String skillsString,String locationsString,int minExp,int maxExp){
		String queryString="";
		String locStr = prepareSearchAlgorForLoc(locationsString);
		String skillStr = prepareSearchAlgorForSkill(skillsString);
		String expStr = prepareSearchAlgorForExperience(minExp,maxExp);
		queryString = locStr+skillStr+expStr;
		System.out.println(queryString);
		return queryString;
	}
	public static String prepareSearchAlgorForLoc(String str){
		StringBuffer buf = new StringBuffer("");
		String s="jobLoc";
		str = str.replaceAll(",", " ");
		String[] splited = str.split("\\s+");
		for (int i = 0; i < splited.length; i++) {
			 buf.append(s);
			 buf.append("='");
			 buf.append(splited[i].trim());
			 buf.append("' or ");
		}
		String s1 = buf.toString();
		String s2 = buf.toString();
		String s3 = buf.toString();
		String s4 = buf.toString();
		
		if(splited.length-1!=0){
			String loc1 = "loc1";
			String loc1Result = s1.replaceAll("loc", loc1);
			buf.append(loc1Result);
		}if(splited.length-2>=0){
			String loc2 = "loc2";
			String loc2Result = s2.replaceAll("loc", loc2);
			buf.append(loc2Result);
		}if(splited.length-3>=0){
			String loc3 = "loc3";
			String loc3Result = s3.replaceAll("loc", loc3);
			buf.append(loc3Result);
		}if(splited.length-4>=0){
			String loc4 = "loc4";
			String loc4Result = s4.replaceAll("loc", loc4);
			buf.append(loc4Result);
		}
		String ss = StringUtils.substring(buf.toString(), 0, buf.toString().length()-3);
		buf = new StringBuffer(ss);
		buf.append("and ");
		//System.out.println(buf.toString());
		return buf.toString();
	}
	
	public static String prepareSearchAlgorForSkill(String skillsString){
		StringBuffer buf = new StringBuffer();
		String s="keySkills";
		skillsString = skillsString.replaceAll(",", " ");
		String[] splited = skillsString.split("\\s+");
		for (int i = 0; i < splited.length; i++) {
			buf.append(s);
			buf.append("='");
			buf.append(splited[i].trim());
			buf.append("' or ");
		}
		String ss = StringUtils.substring(buf.toString(), 0, buf.toString().length()-3);
		buf = new StringBuffer(ss);
		buf.append("and ");
		return buf.toString();
	}
	
	public static String prepareSearchAlgorForExperience(int minExp,int maxExp){
		String s="minExperience between "+minExp+" and "+maxExp;
		String s1=" or maxExperience between "+minExp+" and "+maxExp;
		return s+s1;
		
	}
	
	public static void splitLocToMultipleLocns1(String str){
		String[] splited = str.split("\\s+");
		for (int i = 0; i < splited.length; i++) {
			if(splited[i] != null){
				System.out.println("string: "+splited[i].trim());	
			}
		}
	}

	public static void splitLocToMultipleLocns(String str){
		String str1 = "Hello,I' m, your, String,  abc   def";
		//String[] splited1 = str1.split(",");
		//String[] splited1 = str1.split("\s+|\s*,\s*|\s*=>\s*");
		
		final String REGEX = "\\s*(\\s|=>|,)\\s*";
		final Pattern p = Pattern.compile(REGEX);
        String[] splited1 = p.split(str1);

		
		for (int i = 0; i < splited1.length; i++) {
			if(splited1[i] != null){
				System.out.println("string: "+splited1[i].trim());	
			}
		}	
	}
	

	public static String[] removeSpecialCharsSpaces(String str){
		System.out.println("___"+str);
		str = str.replaceAll("-", " ");
		str = str.replaceAll("&", " ");
		str = str.replaceAll(";", " ");
		str = str.replaceAll("amp", " ");
		str = str.replaceAll(",", " ");
		String[] splited = str.split("\\s+");
		return splited;
	}
	
	
	public static String removeQuotes(String str){
		System.out.println("___"+str);
		str = str.replaceAll("\"", " ");
		str = str.replaceAll("`", " ");
		return str;
	}
	
	
	public static String buildJobsByDesciplineQuery(String functionalArea){
		StringBuffer sbf = new StringBuffer();
		sbf.append("from EmplrJobPostings jobPost where ");
		String[] keyWords =  removeSpecialCharsSpaces(functionalArea);
		for (int i = 0; i < keyWords.length; i++) {
			if(keyWords[i] != null && !keyWords[i].equals("")){
				sbf.append("jobPost.functionalArea like ");
				sbf.append("'%");
				sbf.append(keyWords[i].trim()+"%' or ");
			}
		}
		String ss = StringUtils.substring(sbf.toString(), 0, sbf.toString().length()-3).trim();
		System.out.println(ss);
		return ss;
	}
	
	/*if(str.trim().equalsIgnoreCase("null"))
		return false;*/
	
	public static boolean isFieldNotEmpty(String str){
		if(str !=null && !str.equals("")){
				if(str.trim().length()>0)
					 return true;
		}
		return false;
	}
	
	
	public static boolean isFieldEmpty(String str){
		if(str !=null){
				if(str.trim().length()>0)
		 return false;
		}
		return true;
	}
	
	public static boolean isFieldBetween3nd32(String str){
		if(str !=null && !str.equals("")){
				if(str.trim().length()<3 ||str.trim().length()>32)
		 return false;
		}
		return true;
	}
	
	public static boolean isFieldBetween3nd12(String str){
		if(str !=null && !str.equals("")){
			if(str.trim().length()<3 ||str.trim().length()>12)
		 return false;
		}
		return true;
	}
	
	public static boolean isFieldBetween3nd64(String str){
		if(str !=null && !str.equals("")){
			if(str.trim().length()<3 ||str.trim().length()>64)
		 return false;
		}
		return true;
	}
	
	public static boolean isFieldBetween3nd128(String str){
		if(str !=null && !str.equals("")){
			if(str.trim().length()<3 ||str.trim().length()>128)
		 return false;
		}
		return true;
	}
	
	public static boolean isFieldBetween32nd256(String str){
		if(str !=null && !str.equals("")){
			if(str.trim().length()<32 ||str.trim().length()>256)
		 return false;
		}
		return true;
	}
	
	public static boolean isFieldBetween12nd256(String str){
		if(str !=null && !str.equals("")){
			if(str.trim().length()<12 ||str.trim().length()>256)
		 return false;
		}
		return true;
	}
	
	
	public static boolean isFieldBetween8nd16(String str){
		if(str !=null && !str.equals("")){
			if(str.trim().length()<8 ||str.trim().length()>16)
		 return false;
		}
		return true;
	}
	
	
	
	
	
	public static void functionalArealUtilBuilder(){
		int i=0;
		for (String string : INDUSTRYTYPE_ARRAY) {
	    	 i++;
	    	 System.out.println("if(functionalArea.equalsIgnoreCase(\""+i+"\")){");
	    	 System.out.println("functionalArea=\""+string+"\"; }");
	     }
	}
	
	public static void industryTypeUtil(){
		int a=0;
		String ab="";
		if(a==1){
			ab="industry";
		}
		
		//SelectItem expItem = new SelectItem("", "Select IndustryType");
		//industryTypeList.add(expItem);
		String noticePeriod = "noticePeriod";
		int i=0;
		     for (String string : INDUSTRYTYPE_ARRAY) {
		    	// expItem = new SelectItem(string, string);
		    	// industryTypeList.add(expItem);
		    	 i++;
		    	 System.out.println("if(industryType.equalsIgnoreCase(\""+i+"\")){");
		    	 System.out.println("industryType=\""+string+"\"; }");
		    	// System.out.println(" }");
		     }
	}
	
	
	private static final String[] INDUSTRYTYPE_ARRAY ={
		"Accounts / Finance / Tax / CS / Audit",
        "Agent",
        "Analytics &amp; Business Intelligence",
        "Architecture / Interior Design",
        "Banking / Insurance",
        "Content / Journalism",
        "Corporate Planning / Consulting",
        "Engineering Design / R&amp;D",
        "Export / Import / Merchandising",
        "Fashion / Garments / Merchandising",
        "Guards / Security Services",
        "Hotels / Restaurants",
        "HR / Administration / PR /IR",
        "IT Software - Application Programming / Maintenance",
        "IT Software - Client Server",
        "IT Software - DBA / Datawarehousing",
        "IT Software - E-Commerce / Internet Technologies",
        "IT Software - Embedded / EDA / VLSI / ASIC / Chip Design",
        "IT Software - ERP / CRM",
        "IT Software - Mainframe",
        "IT Software - Middleware",
        "IT Software - Mobile",
        "IT Software - Network Administration / Security",
        "IT Software - Other",
        "IT Software - QA &amp; Testing",
        "IT Software - SAP",
        "IT Software - System Programming",
        "IT Software - Systems / EDP / MIS",
        "IT Software - Telecom Software",
        "IT Software - Web Design/ Graphic Design / Visualiser",
        "IT- Hardware / Telecom / Technical Staff / Support",
        "ITES / BPO / KPO / Customer Service / Operations",
        "Legal",
        "Marketing / Advertising / MR / PR",
        "Other",
        "Packaging",
        "Pharma / Biotech / Healthcare / Medical / R&amp;D",
        "Production / Maintenance / Quality",
        "Purchase / Logistics / Supply Chain",
        "Sales / BD",
        "Secretary / Front Office / Data Entry",
        "Self Employed / Consultants",
        "Shipping",
        "Site Engineering / Project Management",
        "Teaching / Education",
        "Ticketing / Travel / Airlines",
        "Top Management",
        "TV / Films / Production"
			};

	private static final String[] FUNCTIONALAREA_ARRAY ={
        "Accounts / Finance / Tax / CS / Audit",
        "Agent",
        "Analytics &amp; Business Intelligence",
        "Architecture / Interior Design",
        "Banking / Insurance",
        "Content / Journalism",
        "Corporate Planning / Consulting",
        "Engineering Design / R&amp;D",
        "Export / Import / Merchandising",
        "Fashion / Garments / Merchandising",
        "Guards / Security Services",
        "Hotels / Restaurants",
        "HR / Administration / IR",
        "IT Software - Application Programming / Maintenance",
        "IT Software - Client Server",
        "IT Software - DBA / Datawarehousing",
        "IT Software - E-Commerce / Internet Technologies",
        "IT Software - Embedded / EDA / VLSI / ASIC / Chip Design",
        "IT Software - ERP / CRM",
        "IT Software - Mainframe",
        "IT Software - Middleware",
        "IT Software - Mobile",
        "IT Software - Network Administration / Security",
        "IT Software - Other",
        "IT Software - QA &amp; Testing",
        "IT Software - SAP",
        "IT Software - System Programming",
        "IT Software - Systems / EDP / MIS",
        "IT Software - Telecom Software",
        "IT- Hardware / Telecom / Technical Staff / Support",
        "ITES / BPO / KPO / Customer Service / Operations",
        "Legal",
        "Marketing / Advertising / MR / PR",
        "Other",
        "Packaging",
        "Pharma / Biotech / Healthcare / Medical / R&amp;D",
        "Production / Maintenance / Quality",
        "Purchase / Logistics / Supply Chain",
        "Sales / BD",
        "Secretary / Front Office / Data Entry",
        "Self Employed / Consultants",
        "Shipping",
        "Site Engineering / Project Management",
        "Teaching / Education",
        "Ticketing / Travel / Airlines",
        "Top Management",
        "TV / Films / Production",
        "Web / Graphic Design / Visualiser"
	};
	
	
	
	public static boolean extractDomainFromEmail(String email){
		if(email !=null){
			String[] tokens = email.split("@");
			//System.out.println("______"+tokens[0]+"__________"+tokens[1]);
			String domain = tokens[1];
			
			if (domain.equalsIgnoreCase("gmail.com")
					|| domain.equalsIgnoreCase("mail.com")
					|| domain.equalsIgnoreCase("yahoo.com")
					|| domain.equalsIgnoreCase("yahoo.co.in")
					|| domain.equalsIgnoreCase("yahoo.in")
					|| domain.equalsIgnoreCase("ymail.com")
					|| domain.equalsIgnoreCase("aol.com")
					|| domain.equalsIgnoreCase("rediff.com")
					|| domain.equalsIgnoreCase("rediffmail.com")
					|| domain.equalsIgnoreCase("outlook.com")
					|| domain.equalsIgnoreCase("hotmail.com")
					|| domain.equalsIgnoreCase("live.com")
					|| domain.equalsIgnoreCase("live.in")
					|| domain.equalsIgnoreCase("rocketmail.com")
					|| domain.equalsIgnoreCase("gov.in")
					|| domain.equalsIgnoreCase("gov.org")
					|| domain.equalsIgnoreCase("in.com")
					|| domain.equalsIgnoreCase("yopmail.com")
					|| domain.equalsIgnoreCase("facebook.com")
					|| domain.equalsIgnoreCase("quikr.com")
					|| domain.equalsIgnoreCase("yandex.com")
					|| domain.equalsIgnoreCase("gmx.us")
					|| domain.equalsIgnoreCase("gmx.com")
					|| domain.equalsIgnoreCase("googlemail.com")
					|| domain.equalsIgnoreCase("india.com")
					|| domain.equalsIgnoreCase("vsnl.com")
					|| domain.equalsIgnoreCase("bsnl.com")
					|| domain.equalsIgnoreCase("bsnl.in")
					|| domain.equalsIgnoreCase("shahjobs.com")
					|| domain.equalsIgnoreCase("gamil.com")
					|| domain.equalsIgnoreCase("gmial.com")
					|| domain.equalsIgnoreCase("gmail.cm")
					|| domain.equalsIgnoreCase("gmil.com")
					|| domain.equalsIgnoreCase("gmil.co")
					|| domain.equalsIgnoreCase("yahoo.co")
					|| domain.equalsIgnoreCase("pvtltd.in")
					|| domain.equalsIgnoreCase("JobsBazaar.com")
					|| domain.equalsIgnoreCase("satyam.com")
					|| domain.equalsIgnoreCase("usa.net")
					|| domain.equalsIgnoreCase("tm.net.my")
					|| domain.equalsIgnoreCase("shine.com")
								) {
				return false;
			}
			
			return true;
		}
		return false;
	}
	
	public static boolean isSameDomain(String sourcEmail,String email){
			String[] sourcetokens = sourcEmail.split("@");
			String[] tokens = email.split("@");
			String sourcedomain = sourcetokens[1];
			String domain = tokens[1];
			if(sourcedomain.equalsIgnoreCase(domain))
				return true;
			return false;
	}
	
	
	public static String generateRandomString() {
		Random rng = new Random();
		String characters="abcdefghijklmnopqrstuvwxyz123456789"; 
		int length=24;
	    char[] text = new char[length];
	    for (int i = 0; i < length; i++) 	    {
	        text[i] = characters.charAt(rng.nextInt(characters.length()));
	    }
	    System.out.println(text);
	    return new String(text);
	}
	
	public static String generateRandomNumer(Random rng, String characters, int length) 	{
		characters ="123456789";
		length =2;
	    char[] text = new char[length];
	    for (int i = 0; i < length; i++) 	    {
	        text[i] = characters.charAt(rng.nextInt(characters.length()));
	    }
	    System.out.println(text);
	    return new String(text);
	}
	
	
	public static void verifyURL(){
		//String url = "http://www.jumbojobs.com/rfRes/datascroller.ecss.htm";
		//String url = "http://www.jumbojobs.com/javax.faces.resource/jsf.js.htm";
		String url = "http://www.jumbojobs.com/employer/employer-home.htm";
		String[] params = url.split("/");
		//System.out.println("LENGTH:"+params.length);
		if(params.length>3 && (params[3].equals("jobskr")||params[3].equals("employer")||params[3].equals("admin"))){
		System.out.println(params[3]);
		}else{
			System.out.println("Executing all");
		}
	}
	
	
	public static void getUrlValues(){
	String url ="http://localhost:3030/jumbojobsQAenv/common/jsk-forgot-pass-new-pass-update.htm?token=b6lck8vfmncb9pbwq6kaso5n&email=1234@gmail.com";	
	String[] values = url.split("token");
	values = values[1].split("email");
	for (int i = 0; i < values.length; i++) {
		String string = values[i];
		System.out.println(string.substring(1, string.length()));
	}
	}
	
	
	public static boolean onlyNumbers(String inputString) {
		boolean result = false;  
		Pattern pattern = Pattern.compile("^[0-9]+$");  
		        Matcher matcher = pattern.matcher(inputString);  
		        if(matcher.find()){
		        	 result = true;  
		        }else{
		        	result = false; 
		        }
		         return  result;
			}
	
	public static boolean webURL(String inputString) {
		boolean result = false;  
		Pattern pattern = Pattern.compile("(@)?(href=')?(HREF=')?(HREF=\")?(href=\")?(http://)?[a-zA-Z_0-9\\-]+(\\.\\w[a-zA-Z_0-9\\-]+)+(/[#&\\n\\-=?\\+\\%/\\.\\w]+)?");  
		        Matcher matcher = pattern.matcher(inputString);  
		        if(matcher.find()){
		        	 result = true;  
		        }else{
		        	result = false; 
		        }
		         return  result;
			}
	
	public static boolean onlyCharacters(String inputString) {
		boolean result = false;  
		Pattern pattern = Pattern.compile("^[a-zA-Z\\s\\s]+$");  
		        Matcher matcher = pattern.matcher(inputString);  
		        if(matcher.find()){
		        	 result = true;  
		        }else{
		        	result = false; 
		        }
		         return  result;
			}
	
	public  static void splitStringOnSpaceComma(String args) {
	       // this is my string
		 String s = "Java";
	   // String s = "Java,   Programming is ,,, fun do you agree? ."; 
	    String regexp = "[\\s,;\\n\\t]+"; // these are my delimiters
	    String [] tokens; // here i will save tokens
	    
	    //jobpost.keySkills like '%"+keySkills+"%'
	   // StringBuffer sbf=new StringBuffer();
	        tokens = s.split(regexp);
	       /* for (int j = 0; j < tokens.length; j++) {
	        	//System.out.println(tokens[j]);
			}*/
	        
	   }
	
	
	public  static String[] splitStringOnSpaceCommas(String str) {
	       // this is my string
	    //String s = "Java,   Programming is ,,, fun do you agree? ."; 
	    String regexp = "[\\s,;\\n\\t]+"; // these are my delimiters
	    String [] tokens; // here i will save tokens
	    // length is 7 because i know that my string will consist of 7 tokens
	        tokens = str.split(regexp);
	        return tokens;
	   }
	
	
	public static String extractDomainFromEmaill(String email){
		if(email!=null && email.contains("@")){
		String[] tokens = email.split("@");
		return tokens[1];
		}
		return "#####";
	}
	
	
	/*public List<EmplrJobPostings> searchJobsByProperty(String skills,
			String searchTyp) {
		log.info("userid--getDetailsOfCreditCardUserId---" + searchTyp);
		List<EmplrJobPostings> jobsList = null;
		Query qry = null;
		Session session = getHibernateSession();
		StringBuffer hqlQry = new StringBuffer(
				"select * from EmplrJobPostings jbPst where");
		if (searchTyp.equals("Skills")) {
			hqlQry.append("jbPst.skills =?");
			qry = session.createQuery(hqlQry.toString());
			qry.setString(0, "userName");
		}
		if (searchTyp.equals("Location")) {
			hqlQry.append("jbPst.Location =?");
			qry = session.createQuery(hqlQry.toString());
			qry.setString(0, "location");
		}
		return jobsList = qry.list();
	}*/
	
	
	//List<EmplrJobPostings>
	public static String createSearchJobsByPropertyQueryString(Map<String,Object> parmetersMap) {
		StringBuffer hqlQry = new StringBuffer("from JskPrflDtl jskPrfl where ");
		 if (parmetersMap != null && !parmetersMap.isEmpty()) {
	          for (String key: parmetersMap.keySet()) {
	        	  if(key.equals("keySkills"))
	        		  hqlQry.append("jskPrfl.keySkills like '%"+parmetersMap.get("keySkills")+"%'");
	        		  if(key.equals("location"))
	        			  hqlQry.append(" and jskPrfl.location='"+parmetersMap.get("location")+"'");  
	        		  if(key.equals("minExperience"))
	        			  hqlQry.append(" and jskPrfl.expYears between "+parmetersMap.get("minExperience"));
	        		  if(key.equals("maxExperiene"))
	        			  hqlQry.append(" and "+parmetersMap.get("maxExperiene"));
	        		  if(key.equals("minSalary"))
	        			  hqlQry.append(" and jskPrfl.currSalInLachs between "+parmetersMap.get("minSalary"));
	        		  if(key.equals("maxSalary"))
	        			  hqlQry.append(" and "+parmetersMap.get("maxSalary"));
	          }
	          if(!hqlQry.toString().equals("from JskPrflDtl jskPrfl where "))
    			  hqlQry.append(" and ");
	      }
		 System.out.println(hqlQry.toString());
		 return hqlQry.toString();
	}
	
	
	public static void buildEmailContent() {
		File file = new File("files/emailcontent.txt");
		File wrtFile = new File("files/writeText.txt");
		StringBuffer contents = new StringBuffer();
		BufferedReader reader = null;
		BufferedWriter writer = null;
		Map<String, String> map = new HashedMap();
		try {
			reader = new BufferedReader(new FileReader(file));
			writer = new BufferedWriter(new FileWriter(wrtFile));
			String text = null;
			int i=1;
			
			while ((text = reader.readLine()) != null) {
				if(StringUtils.isNotEmpty(text))
					text = text.replaceAll("\"", Matcher.quoteReplacement("\\\"")).trim();
				    System.out.println("sbf.append(\""+text+"\");");
				    contents.append(text).append(System.getProperty("line.separator"));
				    map.put(text, text);
			}
			writer.write(contents.toString());
			writer.flush();
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// show file contents here
		for (Iterator iterator = map.keySet().iterator(); iterator.hasNext();) {
			String  str = (String) iterator.next();
			//System.out.println(str);
			//urlsList.add(e)
		//	if(str.length()>0)
				//System.out.println(str.length());
				//str = str.replaceAll("(?m)^\\s*$[\n\r]{1,}", "");
			//	System.out.println(str);
			//System.out.println("urlsList.add(\""+str+"\");");
		}
		//System.out.println(contents.toString());
	}




public static void datePatternChecker(){
	List<String> dates = new ArrayList<String>();
	//With leading zeros
	dates.add("01/01/11");
	dates.add("01/01/2011");
	dates.add("01/13/2011");
	dates.add("13/13/2011");
	//Missing leading zeros
	dates.add("1/1/11");
	dates.add("01/1/2011");

	String regex = "^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$";
	Pattern pattern = Pattern.compile(regex);

	for(String date : dates) 	{
		Matcher matcher = pattern.matcher(date);
		System.out.println(date +" : "+ matcher.matches());
	}

}



public void testTheads(){
	
}

	public static void main(String[] args) {
		 
		Map<String,Object> parmetersMap = new LinkedHashMap<String, Object>();
		parmetersMap.put("keySkills", "java");
		//parmetersMap.put("location", "pune");
		parmetersMap.put("minExperience", "2");
		parmetersMap.put("maxExperiene", "9");
		parmetersMap.put("minSalary", "6");
		parmetersMap.put("maxSalary", "12");
		createSearchJobsByPropertyQueryString(parmetersMap);
		
		if(2<3)
			System.out.println("ddd");
		
		datePatternChecker();
	}
	
	
}


